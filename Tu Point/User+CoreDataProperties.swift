//
//  User+CoreDataProperties.swift
//  Tu Point
//
//  Created by Victor Salazar on 23/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension User{
    @NSManaged var age:NSNumber?
    @NSManaged var civil_status:String
    @NSManaged var city_id:NSNumber?
    @NSManaged var data_public:NSNumber
    @NSManaged var email:String
    @NSManaged var fb_id:NSNumber?
    @NSManaged var id:NSNumber
    @NSManaged var me:NSNumber
    @NSManaged var name:String
    @NSManaged var parse_id:String?
    @NSManaged var phone:String?
    @NSManaged var photo_url:String?
    @NSManaged var sex:String
    @NSManaged var type:String
    @NSManaged var in_events:String?
    @NSManaged var event_lists:String?
    @NSManaged var preferences:String
}