//
//  MyTicketTableViewCell.swift
//  Tu Point
//
//  Created by victor salazar on 28/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyTicketTableViewCell:UITableViewCell{
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var eventImgView:LoadingImageView!
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var desciptionLbl:UILabel!
}