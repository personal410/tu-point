//
//  Event.swift
//  Tu Point
//
//  Created by Victor Salazar on 28/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Event:NSManagedObject{
    class func createEvent(_ dicEvent:Dictionary<String, AnyObject>) -> Event? {
        let ctxt = AppDelegate.getMoc()
        let newEvent = NSEntityDescription.insertNewObject(forEntityName: "Event", into: ctxt) as! Event
        newEvent.date_time_start = ToolBox.convertStringToDate(dicEvent["date_time_start"] as! String)
        newEvent.date_time_finish = ToolBox.convertStringToDate(dicEvent["date_time_finish"] as! String)
        newEvent.update_time = ToolBox.convertStringToDate(dicEvent["updated_at"] as! String)
        newEvent.eveDescription = dicEvent["description"] as! String
        var id:Int = 0
        if let numberId = dicEvent["id"] as? NSNumber {
            id = numberId.intValue
        }else if let stringId = dicEvent["id"] as? NSString {
            id = stringId.integerValue
        }
        newEvent.id = NSNumber(integerLiteral: id)
        newEvent.name = dicEvent["name"] as! String
        if let numberGuests = dicEvent["number_guest"] as? NSString {
            newEvent.number_guests = NSNumber(integerLiteral: numberGuests.integerValue)
        }else{
            newEvent.number_guests = NSNumber(integerLiteral: 0)
        }
        newEvent.photo_url = dicEvent["photo_url"] as! String
        newEvent.type = dicEvent["type"] as! String
        var cityId:Int = 0
        if let numberCityId = dicEvent["city_id"] as? NSNumber {
            cityId = numberCityId.intValue
        }else if let stringCityId = dicEvent["city_id"] as? NSString {
            cityId = stringCityId.integerValue
        }
        var closed_lists:Int = 0
        if let closedLists = dicEvent["closed_lists"] as? NSNumber {
            closed_lists = closedLists.intValue
        }else if let strClosedLists = dicEvent["closed_lists"] as? NSString {
            closed_lists = strClosedLists.integerValue
        }
        newEvent.closed_lists = NSNumber(booleanLiteral: (closed_lists == 1))
        var public_promoters:Int = 0
        if let publicPromoters = dicEvent["public_promoters"] as? NSNumber {
            public_promoters = publicPromoters.intValue
        }else if let strPublicPromoters = dicEvent["public_promoters"] as? NSString {
            public_promoters = strPublicPromoters.integerValue
        }
        newEvent.public_promoters = NSNumber(booleanLiteral: (public_promoters == 1))
        newEvent.city_id = NSNumber(integerLiteral: cityId)
        newEvent.statistics = EventStatistics.createEventStatistics(dicEvent["statistics"] as! Dictionary<String, AnyObject>)
        if let arrDiscos = Disco.getDiscos() {
            var findDisco = false
            for disco in arrDiscos {
                if disco.id.intValue == (dicEvent["disco_id"] as! NSString).integerValue {
                    newEvent.disco = disco
                    findDisco = true
                    break
                }
            }
            if !findDisco {
                let dicDisco = dicEvent["disco"] as! Dictionary<String, AnyObject>
                let newDisco = NSEntityDescription.insertNewObject(forEntityName: "Disco", into: ctxt) as! Disco
                newDisco.id = NSNumber(integerLiteral: (dicDisco["id"] as! NSString).integerValue)
                newDisco.address = dicDisco["address"] as! String
                newDisco.name = dicDisco["name"] as! String
                newDisco.city_id = NSNumber(integerLiteral: (dicDisco["city_id"] as! NSString).integerValue)
                newDisco.phone = dicDisco["phone"] as! String
                newDisco.latitude = NSNumber(floatLiteral: (dicDisco["latitude"] as! NSString).doubleValue)
                newDisco.longitude = NSNumber(floatLiteral: (dicDisco["longitude"] as! NSString).doubleValue)
                newEvent.disco = newDisco
            }
        }
        if let arrPhotos = dicEvent["photos"] as? Array<Dictionary<String, String>> {
            for dicPhoto in arrPhotos {
                let photoUrl = dicPhoto["photo_url"]!
                EventPhoto.createEventPhoto(photoUrl, event: newEvent)
            }
        }
        do{
            try ctxt.save()
            return newEvent
        }catch let error as NSError {
            print("createEvent error: \(error.description)")
            return nil
        }
    }
    class func editEvent(_ dicEvent:Dictionary<String, AnyObject>) -> Bool {
        let ctxt = AppDelegate.getMoc()
        let newEvent = self.getEventById((dicEvent["id"] as! NSString).integerValue)!
        let newUpdateTime = ToolBox.convertStringToDate(dicEvent["updated_at"] as! String)
        if newUpdateTime.compare(newEvent.update_time) == .orderedDescending {
            let url = URL(string: newEvent.photo_url)!
            let filename = url.lastPathComponent
            let imageDirPath = ToolBox.getImagesDirectory()
            let imagePath = "\(imageDirPath)/\(filename)"
            do{
                try FileManager.default.removeItem(atPath: imagePath)
            }catch let error as NSError {
                print("editEvent error: \(error.description)")
            }
            newEvent.date_time_start = ToolBox.convertStringToDate(dicEvent["date_time_start"] as! String)
            newEvent.date_time_finish = ToolBox.convertStringToDate(dicEvent["date_time_finish"] as! String)
            newEvent.update_time = newUpdateTime
            newEvent.eveDescription = dicEvent["description"] as! String
            newEvent.name = dicEvent["name"] as! String
            newEvent.photo_url = dicEvent["photo_url"] as! String
            newEvent.type = dicEvent["type"] as! String
            newEvent.number_guests = NSNumber(integerLiteral: (dicEvent["number_guests"] as! NSString).integerValue)
            var closed_lists:Int = 0
            if let closedLists = dicEvent["closed_lists"] as? NSNumber {
                closed_lists = closedLists.intValue
            }else if let strClosedLists = dicEvent["closed_lists"] as? NSString {
                closed_lists = strClosedLists.integerValue
            }
            newEvent.closed_lists = NSNumber(booleanLiteral: (closed_lists == 1))
            var public_promoters:Int = 0
            if let publicPromoters = dicEvent["public_promoters"] as? NSNumber {
                public_promoters = publicPromoters.intValue
            }else if let strPublicPromoters = dicEvent["public_promoters"] as? NSString {
                public_promoters = strPublicPromoters.integerValue
            }
            newEvent.public_promoters = NSNumber(booleanLiteral: (public_promoters == 1))
            EventStatistics.updateEventStatitics(dicEvent["statistics"] as! Dictionary<String, AnyObject>, eventStatistics: newEvent.statistics)
            if let arrDiscos = Disco.getDiscos() {
                var findDisco = false
                for disco in arrDiscos {
                    if disco.id.intValue == (dicEvent["disco_id"] as! NSString).integerValue {
                        newEvent.disco = disco
                        findDisco = true
                        break
                    }
                }
                if !findDisco {
                    let dicDisco = dicEvent["disco"] as! Dictionary<String, AnyObject>
                    let newDisco = NSEntityDescription.insertNewObject(forEntityName: "Disco", into: ctxt) as! Disco
                    newDisco.id = NSNumber(integerLiteral: (dicDisco["id"] as! NSString).integerValue)
                    newDisco.address = dicDisco["address"] as! String
                    newDisco.name = dicDisco["name"] as! String
                    newDisco.city_id = NSNumber(integerLiteral: (dicDisco["city_id"] as! NSString).integerValue)
                    newDisco.phone = dicDisco["phone"] as! String
                    newDisco.latitude = NSNumber(floatLiteral: (dicDisco["latitude"] as! NSString).doubleValue)
                    newDisco.longitude = NSNumber(floatLiteral: (dicDisco["longitude"] as! NSString).doubleValue)
                    newEvent.disco = newDisco
                }
            }
            do{
                try ctxt.save()
                return true
            }catch let error as NSError {
                print("editEvent error: \(error.description)")
                return false
            }
        }else{
            return true
        }
    }
    class func getEvents() -> Array<Event>? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<Event>(entityName: "Event")
        fetchReq.predicate = NSPredicate(format: "show_event = 1")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "date_time_start", ascending: true)]
        do{
            let results = try ctxt.fetch(fetchReq) 
            return results
        }catch let error as NSError {
            print("getEvents error: \(error.description)")
            return nil
        }
    }
    class func getAllEvents() -> Array<Event>? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<Event>(entityName: "Event")
        do{
            let results = try ctxt.fetch(fetchReq)
            return results
        }catch let error as NSError {
            print("getEvents error: \(error.description)")
            return nil
        }
    }
    class func getMyEvents() -> Array<Event>? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<Event>(entityName: "Event")
        fetchReq.predicate = NSPredicate(format: "is_my_event = 1")
        do{
            let results = try ctxt.fetch(fetchReq)
            return results
        }catch let error as NSError {
            print("getMyEvents error: \(error.description)")
            return nil
        }
    }
    class func getEventById(_ id:Int) -> Event? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<Event>(entityName: "Event")
        fetchReq.predicate = NSPredicate(format: "id = %i", id)
        do{
            let results = try ctxt.fetch(fetchReq)
            return results.first
        }catch let error as NSError {
            print("getEventById error: \(error.description)")
            return nil
        }
    }
    class func setMyEventToEventId(_ id:Int){
        if let event = self.getEventById(id) {
            event.is_my_event = true
            let ctxt = AppDelegate.getMoc()
            do{
                try ctxt.save()
            }catch let error as NSError {
                print("editEvent error: \(error.description)")
            }
        }
    }
    class func setDontShowToEvent(_ event:Event){
        event.show_event = false
        let ctxt = AppDelegate.getMoc()
        do{
            try ctxt.save()
        }catch let error as NSError {
            print("editEvent error: \(error.description)")
        }
    }
    class func deleteEventWithId(_ id:Int){
        let ctxt = AppDelegate.getMoc()
        let event = self.getEventById(id)
        if event != nil {
            ctxt.delete(event!)
            do{
                try ctxt.save()
            }catch let error as NSError {
                print("deleteEventWithId error: \(error.description)")
            }
        }
    }
}
