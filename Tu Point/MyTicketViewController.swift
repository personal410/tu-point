//
//  MyTicketViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 29/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyTicketViewController:CustomViewController{
    //MARK: - Variables
    var dicTicket:Dictionary<String, AnyObject>!
    //MARK: - IBOutlet
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var descriptionLbl:UILabel!
    @IBOutlet weak var eventImgView:LoadingImageView!
    @IBOutlet weak var qrCodeImgView:LoadingImageView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicEvent = dicTicket["event"] as! Dictionary<String, AnyObject>
        let strDateTimeStart = dicEvent["date_time_start"] as! String
        let dateTimeStart = ToolBox.convertStringToDate(strDateTimeStart)
        let strDateTimeStartFinal = ToolBox.convertDateToString(dateTimeStart, withFormat: "d 'de' MMMM 'del' YYYY")
        var arrStrDateTimeStartFinal = strDateTimeStartFinal.components(separatedBy: " ")
        arrStrDateTimeStartFinal[2] = arrStrDateTimeStartFinal[2].capitalized
        self.dateLbl.text = arrStrDateTimeStartFinal.joined(separator: " ")
        self.eventImgView.image = nil
        self.eventImgView.strUrl = dicEvent["photo_url"] as! String
        let dicDisco = dicEvent["disco"] as! Dictionary<String, String>
        self.titleLbl.text = dicDisco["name"]
        self.descriptionLbl.text = dicEvent["name"] as? String
        self.qrCodeImgView.strUrl = dicTicket["qr_code_url"] as! String
    }
}
