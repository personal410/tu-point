//
//  AdminListPromotersViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 22/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminListPromotersViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate{
    //MARK: - Variables
    var arrDicPromoters:Array<Dictionary<String, AnyObject>> = []
    var eventId:Int = -1
    //MARK: - IBOutlet
    @IBOutlet weak var promotersTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let action = self.eventId == -1 ? "getPromoters" : "getPromotersForEvent"
        var dicParams = ["action": action]
        if self.eventId == -1 {
            dicParams["user_id"] = "\(User.getUser()!.id.intValue)"
        }else{
            dicParams["event_id"] = "\(eventId)"
        }
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicPromotersTemp = dicResult["promoters"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicPromoters = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicPromotersTemp)
                            }
                            SVProgressHUD.dismiss()
                            self.promotersTableView.reloadData()
                            return
                        }else{
                            SVProgressHUD.dismiss()
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            return
                        }
                    }
                }
            }
            SVProgressHUD.dismiss()
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    override func prepare(for segue:UIStoryboardSegue, sender: Any?){
        if let viewCont = segue.destination as? AdminPromoterProfileViewController {
            viewCont.dicPromoter = self.arrDicPromoters[(self.promotersTableView.indexPathForSelectedRow! as NSIndexPath).row]
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrDicPromoters.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "promoterCell", for: indexPath) as! AdminPromoterTableViewCell
        let dicPromoter = self.arrDicPromoters[(indexPath as NSIndexPath).row]
        let dicUser = dicPromoter["user"] as! Dictionary<String, AnyObject>
        cell.promoterImgView.strUrl = dicUser["photo_url"] as! String
        cell.promoterNameLbl.text = dicUser["name"] as? String
        cell.numberAssistantsLbl.text = "\((dicPromoter["average_attendees"] as! NSNumber))"
        cell.totalNumberAssistantsLbl.text = "\((dicPromoter["average_guests"] as! NSNumber))"
        cell.numberListsLbl.text = "\(dicPromoter["lists"] as! NSNumber)"
        let ranking = (dicPromoter["ranking"] as! NSNumber).intValue
        cell.rankingLbl.text = "\(ranking)%"
        if ranking > 50 {
            cell.rankingLbl.textColor = UIColor.white
        }else{
            cell.rankingLbl.textColor = UIColor(r: 229, g: 23, b: 32)
        }
        return cell
    }
}
