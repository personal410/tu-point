//
//  AppDelegate.swift
//  Tu Point
//
//  Created by victor salazar on 15/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import CoreData
import FBSDKCoreKit
import GoogleMaps
import Firebase
import UserNotifications
@UIApplicationMain class AppDelegate:UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    //MARK: - Variables
    var window:UIWindow?
    //MARK: - Events
    class func getMoc() -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.managedObjectContext
    }
    func application(_ application:UIApplication, didFinishLaunchingWithOptions launchOptions:[UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        application.applicationIconBadgeNumber = 0
        GMSServices.provideAPIKey("AIzaSyCpRxoxCqEGenRUISLuPROdBAaICh7Q-TY")
        FIRAnalyticsConfiguration.sharedInstance().setAnalyticsCollectionEnabled(false)
        let imagesDirec = ToolBox.getImagesDirectory()
        if !FileManager.default.fileExists(atPath: imagesDirec, isDirectory: nil) {
            try! FileManager.default.createDirectory(atPath: imagesDirec, withIntermediateDirectories: false, attributes: nil)
        }
        let imagesDirecURL = URL(fileURLWithPath: imagesDirec, isDirectory: true)
        var isExcludedFromBackup:AnyObject?
        try! (imagesDirecURL as NSURL).getResourceValue(&isExcludedFromBackup, forKey: URLResourceKey.isExcludedFromBackupKey)
        if let numIsExcludedFromBackup = isExcludedFromBackup as? NSNumber {
            if !numIsExcludedFromBackup.boolValue {
                try! (imagesDirecURL as NSURL).setResourceValue(true, forKey: URLResourceKey.isExcludedFromBackupKey)
            }
        }
        FIRApp.configure()
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.black)
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        
        if #available(iOS 10.0, *) {
            let authOptions:UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(options: authOptions, completionHandler:{(b:Bool, e:Error?) in })
            UNUserNotificationCenter.current().delegate = self
        }else{
            let userNotificationsTypes:UIUserNotificationType = [.alert, .badge, .sound]
            let settings = UIUserNotificationSettings(types: userNotificationsTypes, categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        if let currentUser = User.getUser() {
            if !UserDefaults.standard.bool(forKey: "com.carlos.tupoint.registerPush") {
                application.registerForRemoteNotifications()
            }
            if currentUser.type == "admin" {
                let adminStoryboard = UIStoryboard(name: "Admin", bundle: nil)
                let viewCont = adminStoryboard.instantiateViewController(withIdentifier: "MainNavCont")
                window!.rootViewController = viewCont
                window?.makeKeyAndVisible()
            }else{
                let adminStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let viewCont = adminStoryboard.instantiateViewController(withIdentifier: "MainViewCont")
                window!.rootViewController = viewCont
                window?.makeKeyAndVisible()
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(tokenRefreshNotification(notification:)), name: NSNotification.Name.firInstanceIDTokenRefresh, object: nil)
        
        return true
    }
    func application(_ application:UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken:Data){
        //let token = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        //print("token: \(token)")
        FIRInstanceID.instanceID().setAPNSToken(deviceToken, type: FIRInstanceIDAPNSTokenType.prod)
        UserDefaults.standard.set(true, forKey: "com.carlos.tupoint.registerPush")
        UserDefaults.standard.synchronize()
    }
    func application(_ application:UIApplication, didFailToRegisterForRemoteNotificationsWithError error:Error){
        print("didFailToRegisterForRemoteNotificationsWithError: \(error)")
    }
    func application(_ application:UIApplication, didReceiveRemoteNotification userInfo:[AnyHashable: Any]){
        handleUserInfo(userInfo: userInfo)
    }
    func application(_ application:UIApplication, didRegister notificationSettings:UIUserNotificationSettings){
        if notificationSettings.types == UIUserNotificationType() {
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Notificaciones deshabilitadas", inViewCont: self.window!.rootViewController!)
        }
    }
    func applicationWillResignActive(_ application:UIApplication){
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application:UIApplication){
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application:UIApplication){
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application:UIApplication){
        application.applicationIconBadgeNumber = 0
    }
    func applicationWillTerminate(_ application:UIApplication){
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }
    //MARK: - Core Data
    lazy var applicationDocumentsDirectory:URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.count-1]
    }()
    lazy var managedObjectModel:NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: "Tu_Point", withExtension: "momd")!
        return NSManagedObjectModel(contentsOf: modelURL)!
    }()
    lazy var persistentStoreCoordinator:NSPersistentStoreCoordinator = {
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent("SingleViewCoreData.sqlite")
        var failureReason = "There was an error creating or loading the application's saved data."
        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: [NSMigratePersistentStoresAutomaticallyOption: true,
                NSInferMappingModelAutomaticallyOption: true])
        } catch {
            var dict = [String:AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?
            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: "YOUR_ERROR_DOMAIN", code: 9999, userInfo: dict)
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")
            abort()
        }
        return coordinator
    }()
    lazy var managedObjectContext:NSManagedObjectContext = {
        let coordinator = self.persistentStoreCoordinator
        var managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = coordinator
        return managedObjectContext
    }()
    func saveContext(){
        if managedObjectContext.hasChanges {
            do{
                try managedObjectContext.save()
            }catch{
                let nserror = error as NSError
                NSLog("Unresolved error \(nserror), \(nserror.userInfo)")
                abort()
            }
        }
    }
    //MARK: - Facebook
    func application(_ application:UIApplication, open url:URL, sourceApplication:String?, annotation:Any) -> Bool {
        return FBSDKApplicationDelegate.sharedInstance().application(application, open: url, sourceApplication: sourceApplication!, annotation: annotation)
    }
    func application(_ app:UIApplication, open url:URL, options:[UIApplicationOpenURLOptionsKey:Any]) -> Bool {
        if #available(iOS 9.0, *) {
            let sourceApplication = options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String
            return FBSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: sourceApplication!, annotation: nil)
        }else{
            return false
        }
    }
    func application(_ application:UIApplication, handleOpen url:URL) -> Bool {
        return true
    }
    //MARK: - UserNotification
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center:UNUserNotificationCenter, didReceive response:UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void){
        handleUserInfo(userInfo: response.notification.request.content.userInfo)
        completionHandler()
    }
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center:UNUserNotificationCenter, willPresent notification:UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void){}
    //MARK: - Auxiliar
    func handleUserInfo(userInfo:[AnyHashable: Any]){
        if User.getUser() != nil {
            print("userInfo: \(userInfo)")
            if let type = userInfo["type"] as? String {
                print("type: *\(type)*")
                if type == "event" {
                    if let object = userInfo["object"] as? String {
                        if let dataObject = object.data(using: .utf8) {
                            do{
                                let jsonObject = try JSONSerialization.jsonObject(with: dataObject, options: .allowFragments)
                                if let dicObject = jsonObject as? Dictionary<String, AnyObject> {
                                    if let id = dicObject["id"] as? NSNumber {
                                        var event = Event.getEventById(id.intValue)
                                        if event == nil{
                                            event = Event.createEvent(dicObject)
                                        }
                                        if event != nil {
                                            print("show event")
                                            if let mainViewCont = window?.rootViewController as? MainViewController {
                                                if let eventDetailViewCont = mainViewCont.storyboard?.instantiateViewController(withIdentifier: "EventDetailViewCont") as? EventDetailViewController {
                                                    eventDetailViewCont.event = event!
                                                    for childViewCont in mainViewCont.childViewControllers {
                                                        if !childViewCont.isKind(of: MenuViewController.self) {
                                                            if let navViewCont = childViewCont as? UINavigationController {
                                                                navViewCont.pushViewController(eventDetailViewCont, animated: true)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }catch let error {
                                print("error: \(error)")
                            }
                        }
                    }
                }else if type == "event_list" {
                    print("event_list")
                    if let object = userInfo["object"] as? String {
                        if let dataObject = object.data(using: .utf8) {
                            do{
                                let jsonObject = try JSONSerialization.jsonObject(with: dataObject, options: .allowFragments)
                                if let dicList = jsonObject as? Dictionary<String, AnyObject> {
                                    if let state = dicList["state"] as? String {
                                        if let mainViewCont = window?.rootViewController as? MainViewController {
                                            for childViewCont in mainViewCont.childViewControllers {
                                                if !childViewCont.isKind(of: MenuViewController.self) {
                                                    if let navViewCont = childViewCont as? UINavigationController {
                                                        if state == "wait" {
                                                            if let checkListViewCont = mainViewCont.storyboard?.instantiateViewController(withIdentifier: "CheckListViewCont") as? CheckListViewController {
                                                                checkListViewCont.eventList = EventList(dicList: dicList)
                                                                navViewCont.pushViewController(checkListViewCont, animated: true)
                                                            }
                                                        }else if state == "accepted" {
                                                            if let finalListViewCont = mainViewCont.storyboard?.instantiateViewController(withIdentifier: "FinalListViewCont") as? FinalListViewController {
                                                                finalListViewCont.eventList = EventList(dicList: dicList)
                                                                navViewCont.pushViewController(finalListViewCont, animated: true)
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }catch let error {
                                print("error: \(error)")
                            }
                        }
                    }
                }else if type == "event_guest" {
                    if let mainViewCont = window?.rootViewController as? MainViewController {
                        mainViewCont.currentIndex = 3
                    }
                }
            }
        }
    }
    func tokenRefreshNotification(notification:NSNotification){
        let refreshedToken = FIRInstanceID.instanceID().token()
        print("refreshedToken: \(refreshedToken)")
    }
}
