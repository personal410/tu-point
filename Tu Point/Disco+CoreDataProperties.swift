//
//  Disco+CoreDataProperties.swift
//  Tu Point
//
//  Created by Victor Salazar on 28/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Disco{
    @NSManaged var address:String
    @NSManaged var city_id:NSNumber
    @NSManaged var id:NSNumber
    @NSManaged var latitude:NSNumber
    @NSManaged var longitude:NSNumber
    @NSManaged var name:String
    @NSManaged var phone:String
    @NSManaged var events:NSSet?
}