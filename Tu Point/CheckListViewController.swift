//
//  CheckListViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 9/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CheckListViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var arrSelectedUserIds:Array<Int> = []
    var arrSelectedVipIds:Array<Int> = []
    var arrCurrentDicGuests:Array<Dictionary<String, AnyObject>> = []
    var arrDicGuests:Array<Dictionary<String, AnyObject>> = []
    var eventList:EventList!
    //MARK: - IBOutlet
    @IBOutlet weak var acceptListBtn:UIButton!
    @IBOutlet weak var rejectListBtn:UIButton!
    @IBOutlet weak var searchTxtFld:UITextField!
    @IBOutlet weak var numberContactsLbl:UILabel!
    @IBOutlet weak var contactsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        for dicGuestTemp in eventList.arrGuest {
            let id = (dicGuestTemp["id"] as! NSString).integerValue
            arrSelectedUserIds.append(id)
            var inserted = false
            if let guestNameTemp = dicGuestTemp["name"] as? String {
                for (index, dicGuest) in self.arrDicGuests.enumerated() {
                    let guestName = dicGuest["name"] as! String
                    if guestNameTemp < guestName {
                        inserted = true
                        self.arrDicGuests.insert(dicGuestTemp, at: index)
                        break
                    }
                }
            }
            if !inserted {
                self.arrDicGuests.append(dicGuestTemp)
            }
        }
        self.arrCurrentDicGuests = self.arrDicGuests
        let btnWidth = (self.view.frame.size.width - 30) / 2.0
        let btnHeight = btnWidth * 8 / 33
        let btnSize = CGSize(width: btnWidth, height: btnHeight)
        self.acceptListBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(btnSize), at: 0)
        self.rejectListBtn.layer.insertSublayer(ToolBox.createRedGradientLayerWithSize(btnSize), at: 0)
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(CheckListViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CheckListViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    @IBAction func addUser(_ btn:UIButton){
        for selectedId in arrSelectedUserIds {
            if selectedId == btn.tag {
                self.arrSelectedUserIds.remove(at: self.arrSelectedUserIds.index(of: selectedId)!)
                self.contactsTableView.reloadData()
                self.updateNumberContactsLbl()
                return
            }
        }
        self.arrSelectedUserIds.append(btn.tag)
        self.contactsTableView.reloadData()
        self.updateNumberContactsLbl()
    }
    @IBAction func addVip(_ btn:UIButton){
        for selectedId in arrSelectedVipIds {
            if selectedId == btn.tag {
                self.arrSelectedVipIds.remove(at: self.arrSelectedVipIds.index(of: selectedId)!)
                self.contactsTableView.reloadData()
                return
            }
        }
        self.arrSelectedVipIds.append(btn.tag)
        self.contactsTableView.reloadData()
    }
    
    @IBAction func sendList(_ btn:UIButton){
        let eventListId = eventList.eventListId
        let action = btn.tag == 1 ? "acceptList" : "rejectList"
        var dicParams = ["action": action, "event_list_id": eventListId]
        if btn.tag == 1 {
            dicParams["users"] = self.arrSelectedUserIds.map(){"\($0)"}.joined(separator: ",")
            dicParams["vips"] = self.arrSelectedVipIds.map(){"\($0)"}.joined(separator: ",")
        }
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            _ = self.navigationController?.popViewController(animated: true)
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    @IBAction func dismissKeyboard(){
        self.searchTxtFld.text = ""
        self.searchTxtFld.resignFirstResponder()
        self.arrCurrentDicGuests = self.arrDicGuests
        self.contactsTableView.reloadData()
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrCurrentDicGuests.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        let dicGuest = self.arrCurrentDicGuests[(indexPath as NSIndexPath).row]
        cell.contactNameLbl.text = dicGuest["name"] as? String
        cell.contactImgView.strUrl = dicGuest["photo_url"] as! String
        let id = (dicGuest["id"] as! NSString).integerValue
        cell.addButton.tag = id
        cell.VIPButton.tag = id
        var vipImg = UIImage(named: "vip")!
        if self.arrSelectedVipIds.index(of: id) == nil {
            vipImg = vipImg.withRenderingMode(.alwaysTemplate)
        }
        cell.VIPButton.setImage(vipImg, for: .normal)
        let addBtnImgName = (self.arrSelectedUserIds.index(of: id) == nil) ? "add" : "added"
        cell.addButton.setImage(UIImage(named: addBtnImgName), for: .normal)
        return cell
    }
    //MARK: - Auxiliar
    func updateNumberContactsLbl(){
        let numberFormattet = NumberFormatter()
        numberFormattet.minimumIntegerDigits = 2
        let number = NSNumber(value: self.arrSelectedUserIds.count as Int)
        self.numberContactsLbl.text = "\(numberFormattet.string(from: number)!) amigos añadidos"
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let totalHeight = self.view.frame.height
        let maxY = self.contactsTableView.frame.maxY
        let bottomSpace = totalHeight - maxY
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight - bottomSpace, right: 0)
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.characters.count == 0 {
            self.arrCurrentDicGuests = self.arrDicGuests
        }else{
            self.arrCurrentDicGuests = self.arrDicGuests.filter(){($0["name"] as! String).range(of: newString, options: .caseInsensitive, range: nil, locale: nil) != nil}
        }
        self.contactsTableView.reloadData()
        return true
    }
}
