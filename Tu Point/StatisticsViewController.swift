//
//  StadisticsViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 2/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class StatisticsViewController:CustomViewController{
    //MARK: - Variables
    var event:Event!
    //MARK: - IBOutlet
    @IBOutlet weak var eventNameLbl:UILabel!
    @IBOutlet weak var discoNameLbl:UILabel!
    @IBOutlet weak var countAlreadyHereLbl:UILabel!
    @IBOutlet weak var triangleImgView:UIImageView!
    @IBOutlet weak var alreadyHereLbl:UILabel!
    @IBOutlet weak var separatorLbl:UILabel!
    @IBOutlet weak var totalGuestLbl:UILabel!
    @IBOutlet weak var menLbl:UILabel!
    @IBOutlet weak var womenLbl:UILabel!
    @IBOutlet weak var undefinedLbl:UILabel!
    @IBOutlet weak var singleLbl:UILabel!
    @IBOutlet weak var inRelationshipLbl:UILabel!
    @IBOutlet weak var committedLbl:UILabel!
    @IBOutlet weak var marriedLbl:UILabel!
    @IBOutlet weak var openRelationLbl:UILabel!
    @IBOutlet weak var undefinedRelationLbl:UILabel!
    @IBOutlet weak var ageRange1Lbl:UILabel!
    @IBOutlet weak var ageRange2Lbl:UILabel!
    @IBOutlet weak var ageRange3Lbl:UILabel!
    @IBOutlet weak var ageRange4Lbl:UILabel!
    @IBOutlet weak var electronicLbl:UILabel!
    @IBOutlet weak var reggaetonLbl:UILabel!
    @IBOutlet weak var salsaLbl:UILabel!
    @IBOutlet weak var pachangaLbl:UILabel!
    @IBOutlet weak var rockLbl:UILabel!
    @IBOutlet weak var cumbiaLbl:UILabel!
    @IBOutlet weak var otherMusicLbl:UILabel!
    @IBOutlet weak var datingLbl:UILabel!
    @IBOutlet weak var friendsLbl:UILabel!
    @IBOutlet weak var meetPeopleLbl:UILabel!
    @IBOutlet weak var justFunLbl:UILabel!
    @IBOutlet weak var whiskyLbl:UILabel!
    @IBOutlet weak var jaggerLbl:UILabel!
    @IBOutlet weak var ronLbl:UILabel!
    @IBOutlet weak var beerLbl:UILabel!
    @IBOutlet weak var piscoLbl:UILabel!
    @IBOutlet weak var waterLbl:UILabel!
    @IBOutlet weak var otherDrinkLbl:UILabel!
    @IBOutlet weak var scrollView:UIScrollView!
    //committed
    @IBOutlet weak var centerXTotalGuestNumberLblCons:NSLayoutConstraint!
    @IBOutlet weak var centerXTotalGuestLblCons:NSLayoutConstraint!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.eventNameLbl.text = self.event.name
        self.discoNameLbl.text = "Discoteca \(self.event.disco.name)"
        if User.getUser()!.type == "user" {
            self.centerXTotalGuestLblCons.priority = 750
            self.centerXTotalGuestNumberLblCons.priority = 750
            self.countAlreadyHereLbl.isHidden = true
            self.triangleImgView.isHidden = true
            self.alreadyHereLbl.isHidden = true
            self.separatorLbl.isHidden = true
        }
        let dicParams = ["action": "getStatisticsForEvent", "event_id": "\(event.id)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let dicStatistics = dicResult["statistics"] as? Dictionary<String, String> {
                                self.countAlreadyHereLbl.text = dicStatistics["assistants"]
                                self.totalGuestLbl.text = dicStatistics["attend"]
                                self.menLbl.text = dicStatistics["men"]
                                self.womenLbl.text = dicStatistics["women"]
                                self.undefinedLbl.text = dicStatistics["undefined"]
                                self.singleLbl.text = dicStatistics["single"]
                                self.inRelationshipLbl.text = dicStatistics["in_relationship"]
                                self.committedLbl.text = dicStatistics["committed"]
                                self.marriedLbl.text = dicStatistics["married"]
                                self.openRelationLbl.text = dicStatistics["open_relation"]
                                self.undefinedRelationLbl.text = dicStatistics["undefined_relation"]
                                self.ageRange1Lbl.text = dicStatistics["range_age_one"]
                                self.ageRange2Lbl.text = dicStatistics["range_age_two"]
                                self.ageRange3Lbl.text = dicStatistics["range_age_three"]
                                self.ageRange4Lbl.text = dicStatistics["range_age_four"]
                                
                                self.electronicLbl.text = dicStatistics["electronic"]
                                self.reggaetonLbl.text = dicStatistics["reggaeton"]
                                self.salsaLbl.text = dicStatistics["salsa"]
                                self.pachangaLbl.text = dicStatistics["pachanga"]
                                self.rockLbl.text = dicStatistics["rock"]
                                self.cumbiaLbl.text = dicStatistics["cumbia"]
                                self.otherMusicLbl.text = dicStatistics["other_music"]
                                
                                self.datingLbl.text = dicStatistics["dating"]
                                self.friendsLbl.text = dicStatistics["friends"]
                                self.meetPeopleLbl.text = dicStatistics["meet_people"]
                                self.justFunLbl.text = dicStatistics["just_fun"]
                                
                                self.whiskyLbl.text = dicStatistics["whisky"]
                                self.jaggerLbl.text = dicStatistics["jagger"]
                                self.ronLbl.text = dicStatistics["ron"]
                                self.beerLbl.text = dicStatistics["beer"]
                                self.piscoLbl.text = dicStatistics["pisco"]
                                self.waterLbl.text = dicStatistics["whater"]
                                self.otherDrinkLbl.text = dicStatistics["other_drink"]
                            }
                            self.scrollView.isHidden = false
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
}
