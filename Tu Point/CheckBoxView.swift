//
//  CheckBoxView.swift
//  Tu Point
//
//  Created by Victor Salazar on 26/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CheckBoxView:UIControl{
    var checked:Bool = true {
        didSet{
            let imgName = self.checked ? "checked" : "unchecked"
            self.checkImgView.image = UIImage(named: imgName)
        }
    }
    let checkImgView:UIImageView
    required init?(coder aDecoder:NSCoder){
        checkImgView = UIImageView()
        super.init(coder: aDecoder)
        checkImgView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.addSubview(checkImgView)
        self.isUserInteractionEnabled = false
    }
}
