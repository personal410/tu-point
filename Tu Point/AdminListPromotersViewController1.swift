//
//  AdminListPromotersViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 22/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminListPromotersViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate{
    //MARK: - Variables
    let arrPromoters:Array<Dictionary<String, AnyObject>> = [["promoterImgName": "user1", "promoterName": "Luis Diaz", "numberAssistants": 97, "totalNumberAssistants": 300, "numberLists": 13, "ranking": 72], ["promoterImgName": "user2", "promoterName": "Carmen Castro", "numberAssistants": 97, "totalNumberAssistants": 300, "numberLists": 13, "ranking": 72], ["promoterImgName": "user1", "promoterName": "Pedro Bravo", "numberAssistants": 97, "totalNumberAssistants": 300, "numberLists": 13, "ranking": 72], ["promoterImgName": "user2", "promoterName": "Laura Dominguez", "numberAssistants": 32, "totalNumberAssistants": 105, "numberLists": 13, "ranking": 14], ["promoterImgName": "user2", "promoterName": "Adriana Mendoza", "numberAssistants": 26, "totalNumberAssistants": 105, "numberLists": 13, "ranking": 33], ["promoterImgName": "user2", "promoterName": "Priscilla Barrios", "numberAssistants": 50, "totalNumberAssistants": 237, "numberLists": 13, "ranking": 19], ["promoterImgName": "user1", "promoterName": "Giancarlo Alvarez", "numberAssistants": 39, "totalNumberAssistants": 174, "numberLists": 13, "ranking": 26], ["promoterImgName": "user1", "promoterName": "Elbert Mujica", "numberAssistants": 31, "totalNumberAssistants": 112, "numberLists": 13, "ranking": 17], ["promoterImgName": "user1", "promoterName": "Julio Gonzales", "numberAssistants": 24, "totalNumberAssistants": 186, "numberLists": 13, "ranking": 20], ["promoterImgName": "user2", "promoterName": "Miriam López", "numberAssistants": 28, "totalNumberAssistants": 193, "numberLists": 13, "ranking": 43]]
    //MARK: - IBAction
    @IBAction func seeProfile(ctrl:UIControl){
        self.performSegueWithIdentifier("showProfile", sender: nil)
    }
    //MARK: - TableView
    func tableView(tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrPromoters.count
    }
    func tableView(tableView:UITableView, cellForRowAtIndexPath indexPath:NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("promoterCell", forIndexPath: indexPath) as! AdminPromoterTableViewCell
        let dicPromoter = self.arrPromoters[indexPath.row]
        cell.promoterImgView.image = UIImage(named: dicPromoter["promoterImgName"] as! String)
        cell.promoterNameLbl.text = dicPromoter["promoterName"] as? String
        cell.numberAssistantsLbl.text = "\(dicPromoter["numberAssistants"] as! Int)"
        cell.totalNumberAssistantsLbl.text = "\(dicPromoter["totalNumberAssistants"] as! Int)"
        cell.numberListsLbl.text = "\(dicPromoter["numberLists"] as! Int)"
        let ranking = dicPromoter["ranking"] as! Int
        cell.rankingLbl.text = "\(ranking)%"
        if ranking > 50 {
            cell.rankingLbl.textColor = UIColor.whiteColor()
        }else{
            cell.rankingLbl.textColor = UIColor(r: 229, g: 23, b: 32)
        }
        cell.seeProfileCtrl.tag = indexPath.row
        return cell
    }
}