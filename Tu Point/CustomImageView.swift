//
//  CustomImageView.swift
//  Tu Point
//
//  Created by victor salazar on 22/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CustomImageView:UIImageView{
    var borderColor:UIColor! = UIColor.clear {
        didSet{
            self.layer.borderWidth = 1
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
