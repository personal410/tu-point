//
//  MyEventTableViewCell.swift
//  Tu Point
//
//  Created by victor salazar on 5/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyEventTableViewCell:UITableViewCell{
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var eventImgView:LoadingImageView!
}