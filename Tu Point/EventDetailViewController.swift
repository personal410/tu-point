//
//  EventDetailViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 29/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class EventDetailViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var dicEvent:Dictionary<String, String>!
    var event:Event!
    var currentList:Int = 0 {
        didSet{
            self.listTableView.reloadData()
        }
    }
    let arrOptionsGoOrSendList = ["Asistir", "Enviar lista", "Cancelar"]
    var shouldShowGoOrSendListButton = true
    //MARK: - IBOutlet
    @IBOutlet weak var eventNameLbl:UILabel!
    @IBOutlet weak var discoNameLbl:UILabel!
    @IBOutlet weak var eventImgView:LoadingImageView!
    @IBOutlet weak var imgViewGradientView:UIView!
    @IBOutlet weak var dateLbl:UILabel!
    @IBOutlet weak var addressLbl:UILabel!
    @IBOutlet weak var descriptionLbl:UILabel!
    @IBOutlet weak var goOrSendListBtn:UIButton!
    @IBOutlet weak var guestListCtrl:UIControl!
    @IBOutlet weak var stadisticsCtrl:UIControl!
    @IBOutlet weak var listGradientView:UIView!
    @IBOutlet weak var listTableView:UITableView!
    @IBOutlet weak var seeMapBottomSpaceLayout:NSLayoutConstraint!
    @IBOutlet weak var closeListsBtn:UIButton!
    @IBOutlet weak var guestListCtrlSpaceCons:NSLayoutConstraint!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.eventNameLbl.text = self.event.name
        self.discoNameLbl.text = self.event.disco.name
        self.eventImgView.strUrl = self.event.photo_url
        let dateTemp = ToolBox.convertDateToString(self.event.date_time_start, withFormat: "d 'de' MMMM 'del' YYYY hh:mm a")
        self.dateLbl.text = dateTemp
        self.addressLbl.text = self.event.disco.address
        self.descriptionLbl.text = self.event.eveDescription
        let colorTop = UIColor.clear.cgColor
        let colorMiddle = UIColor.clear.cgColor
        let colorBottom = UIColor(rgb: 0, a: 0.9).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.origin = CGPoint(x: 0, y: 0)
        gradientLayer.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.width * 140.0 / 375.0)
        gradientLayer.colors = [colorTop, colorMiddle, colorBottom]
        gradientLayer.locations = [0, 0.4, 1]
        self.imgViewGradientView.layer.insertSublayer(gradientLayer, at: 0)
        self.goOrSendListBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(CGSize(width: 250, height: 45)), at: 0)
        self.guestListCtrl.layer.borderColor = UIColor(r: 0, g: 145, b: 228).cgColor
        self.stadisticsCtrl.layer.borderColor = UIColor(r: 0, g: 145, b: 228).cgColor
        
        let colorMiddle2 = UIColor(rgb: 0, a: 0.6).cgColor
        let listGradientLayer = CAGradientLayer()
        listGradientLayer.frame.origin = CGPoint.zero
        listGradientLayer.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height - self.listGradientView.frame.origin.y)
        listGradientLayer.colors = [colorTop, colorMiddle2, colorBottom]
        listGradientLayer.locations = [0, 0.2, 1]
        self.listGradientView.layer.insertSublayer(listGradientLayer, at: 0)
        if User.getUser()!.type == "admin" {
            closeListsBtn.layer.borderColor = UIColor(r: 0, g: 171, b: 255).cgColor
            if event.closed_lists.boolValue {
                closeListsBtn.isEnabled = false
            }
        }else{
            closeListsBtn.isHidden = true
            guestListCtrlSpaceCons.priority = 400
        }
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        self.configureGoOrSendListBtn()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? CreateListViewController {
            viewCont.eventId = event.id.intValue
        }else{
            segue.destination.setValue(self.event, forKey: "event")
        }
    }
    //MARK: - IBAction
    @IBAction func goOrSendList(){
        if User.getUser()!.type == "admin" {
            let adminStoryboard = UIStoryboard(name: "Admin", bundle: nil)
            let adminListPromotersViewCont = adminStoryboard.instantiateViewController(withIdentifier: "adminListPromotersViewCont") as! AdminListPromotersViewController
            adminListPromotersViewCont.eventId = event.id.intValue
            self.navigationController?.pushViewController(adminListPromotersViewCont, animated: true)
        }else{
            if event.type == "free" {
                if !event.closed_lists.boolValue {
                    self.performSegue(withIdentifier: "showCreateList", sender: nil)
                }
            }else if event.type == "list" {
                if !event.closed_lists.boolValue {
                    self.performSegue(withIdentifier: "showChoosePromoter", sender: nil)
                }
            }else{
                self.setAttend()
            }
        }
    }
    @IBAction func showMap(){
        self.performSegue(withIdentifier: "showMap", sender: nil)
    }
    @IBAction func seeGuestList(){
        self.performSegue(withIdentifier: "showGuestList", sender: nil)
    }
    @IBAction func seeStadistics(){
        self.performSegue(withIdentifier: "showStadistics", sender: nil)
    }
    @IBAction func dissmissGradientView(){
        self.hideList(2)
    }
    @IBAction func closedLists(){
        let alertCont = UIAlertController(title: "Alerta", message: "¿Realmente quiere cerrar el envío de listas?", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Cerrar envío", style: .default, handler:{(action:UIAlertAction) in
            let dicParams = ["action": "closeLists", "event_id": "\(self.event.id.intValue)"]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
                SVProgressHUD.dismiss()
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                self.closeListsBtn.isEnabled = false
                            }else{
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            }
                            return
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }))
        self.present(alertCont, animated: true, completion: nil)
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return 3
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! ListOptionTableViewCell
        cell.titleLbl.text = self.arrOptionsGoOrSendList[(indexPath as NSIndexPath).row]
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        self.hideList((indexPath as NSIndexPath).row)
    }
    //MARK: - Auxiliar
    func showList(){
        self.listGradientView.transform = CGAffineTransform(translationX: 0, y: self.listGradientView.frame.height)
        self.listGradientView.isHidden = false
        UIView.animate(withDuration: 0.25, animations: {
            self.listGradientView.transform = CGAffineTransform.identity
        })
    }
    func hideList(_ option:Int){
        UIView.animate(withDuration: 0.25, animations: {
            self.listGradientView.transform = CGAffineTransform(translationX: 0, y: self.listGradientView.frame.height)
        }, completion: {(b:Bool) in
            self.listGradientView.isHidden = true
            self.listGradientView.transform = CGAffineTransform.identity
            if option == 0 {
                self.setAttend()
            }else if option == 1 {
                if self.event.type == "free" {
                    self.performSegue(withIdentifier: "showCreateList", sender: nil)
                }else{
                    self.performSegue(withIdentifier: "showChoosePromoter", sender: nil)
                }
            }
        })
    }
    func configureGoOrSendListBtn(){
        var arrOptions:Array<String> = []
        var buttonTitle = ""
        if User.getUser()!.type != "admin" {
            if event.type == "list" {
                if event.closed_lists.boolValue {
                    arrOptions.append("Envío de listas cerrado")
                }else{
                    arrOptions.append("Enviar Lista")
                }
            }else if event.type == "free" {
                if !event.closed_lists.boolValue {
                    arrOptions.append("Enviar Lista")
                }else{
                    arrOptions.append("Envío de listas cerrado")
                }
            }else{
                let arrInEvents = User.getUser()!.in_events!.components(separatedBy: ",").map(){($0 as NSString).integerValue}
                if arrInEvents.index(of: event.id.intValue) == nil {
                    arrOptions.append("Asistir")
                }else{
                    self.goOrSendListBtn.isHidden = true
                    self.seeMapBottomSpaceLayout.priority = 750
                    return
                }
            }
            buttonTitle = arrOptions.joined(separator: " / ")
        }else{
            buttonTitle = "Promotores del evento"
        }
        self.goOrSendListBtn.setTitle(buttonTitle, for: UIControlState())
    }
    func setAttend(){
        let dicParams = ["action": "setAttend", "event_id": "\(event.id.intValue)", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            self.shouldShowGoOrSendListButton = false
                            User.addEventIdInInEvents(self.event.id.intValue)
                            self.configureGoOrSendListBtn()
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
}
