//
//  ItemListTableViewCell.swift
//  Tu Point
//
//  Created by Victor Salazar on 22/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ItemListTableViewCell:UITableViewCell{
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var checkImgView:UIImageView!
}