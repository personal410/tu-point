//
//  City+CoreDataProperties.swift
//  Tu Point
//
//  Created by victor salazar on 7/04/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension City{
    @NSManaged var id:NSNumber
    @NSManaged var name:String
}