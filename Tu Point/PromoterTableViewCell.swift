//
//  PromoterTableViewCell.swift
//  Tu Point
//
//  Created by Victor Salazar on 11/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class PromoterTableViewCell:UITableViewCell{
    @IBOutlet weak var promoterImgView:FBImageView!
    @IBOutlet weak var promoterNameLbl:UILabel!
}