//
//  GuestListViewController.swift
//  Tu Point
//
//  Created by victor salazar on 5/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class GuestListViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var event:Event!
    var arrCurrentContacts = Array<Dictionary<String, AnyObject>>()
    var arrDicContacts = Array<Dictionary<String, AnyObject>>()
    //MARK: - IBOutlet
    @IBOutlet weak var searchTxtFld:UITextField!
    @IBOutlet weak var numberContactsLbl:UILabel!
    @IBOutlet weak var contactsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(GuestListViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(GuestListViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.arrCurrentContacts = self.arrDicContacts
        let dicParams = ["action": "getEventGuests", "event_id": "\(event.id)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrUsers = dicResult["users"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicContacts = ToolBox.orderDicObjectsByName(arrDicObjects: arrUsers)
                                self.arrCurrentContacts = self.arrDicContacts
                                SVProgressHUD.dismiss()
                                self.contactsTableView.reloadData()
                            }
                            return
                        }else{
                            SVProgressHUD.dismiss()
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            SVProgressHUD.dismiss()
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrCurrentContacts.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        let dicContact = self.arrCurrentContacts[(indexPath as NSIndexPath).row]
        cell.contactImgView.strUrl = dicContact["photo_url"] as! String
        cell.contactNameLbl.text = dicContact["name"] as? String
        return cell
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0)
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.characters.count == 0 {
            self.arrCurrentContacts = self.arrDicContacts
        }else{
            self.arrCurrentContacts = self.arrDicContacts.filter(){($0["name"] as! String).range(of: newString, options: .caseInsensitive, range: nil, locale: nil) != nil}
        }
        self.contactsTableView.reloadData()
        return true
    }
}
