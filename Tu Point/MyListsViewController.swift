//
//  MyListsViewController.swift
//  Tu Point
//
//  Created by victor salazar on 6/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyListsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var arrDicEvents:Array<Dictionary<String, AnyObject>> = []
    //MARK: - IBOutlet
    @IBOutlet weak var myListsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicParams = ["action": "getEventsForLists", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicEventsTemp = dicResult["events"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicEvents = arrDicEventsTemp
                                let currentEvents = Event.getEvents()!
                                var currentEventIds = currentEvents.map(){$0.id.intValue}
                                for dicEvent in arrDicEventsTemp {
                                    let id = (dicEvent["id"] as! NSString).integerValue
                                    if let index = currentEventIds.index(of: id) {
                                        currentEventIds.remove(at: index)
                                        _ = Event.editEvent(dicEvent)
                                    }else{
                                        _ = Event.createEvent(dicEvent)
                                    }
                                }
                                self.myListsTableView.reloadData()
                            }
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? MyListsForEventViewController {
            viewCont.dicEvent = self.arrDicEvents[(self.myListsTableView.indexPathForSelectedRow! as NSIndexPath).row]
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrDicEvents.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myListCell", for: indexPath) as! MyTicketTableViewCell
        let dicEvent = self.arrDicEvents[(indexPath as NSIndexPath).row]
        let strDateTimeStart = dicEvent["date_time_start"] as! String
        let dateTimeStart = ToolBox.convertStringToDate(strDateTimeStart)
        let strDateTimeStartFinal = ToolBox.convertDateToString(dateTimeStart, withFormat: "d 'de' MMMM 'del' YYYY")
        var arrStrDateTimeStartFinal = strDateTimeStartFinal.components(separatedBy: " ")
        arrStrDateTimeStartFinal[2] = arrStrDateTimeStartFinal[2].capitalized
        cell.dateLbl.text = arrStrDateTimeStartFinal.joined(separator: " ")
        cell.eventImgView.image = nil
        cell.eventImgView.strUrl = dicEvent["photo_url"] as! String
        let dicDisco = dicEvent["disco"] as! Dictionary<String, String>
        cell.titleLbl.text = dicDisco["name"]
        cell.desciptionLbl.text = dicEvent["name"] as? String
        return cell
    }
}
