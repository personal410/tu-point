//
//  MenuViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 22/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MenuViewController:UIViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var profileImgView:UIImageView!
    @IBOutlet weak var userNameLbl:UILabel!
    @IBOutlet weak var myListsCtrl:UIControl!
    @IBOutlet weak var bottomSpaceNotificationContCons:NSLayoutConstraint!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.profileImgView.layer.borderColor = UIColor.white.cgColor
        let fbId = User.getUser()!.fb_id!.intValue
        let filename = "\(fbId).png"
        let imageDirPath = ToolBox.getImagesDirectory()
        let imagePath = "\(imageDirPath)/\(filename)"
        self.profileImgView.image = UIImage(contentsOfFile: imagePath)
        self.userNameLbl.text = User.getUser()!.name
        if User.getUser()!.type == "person" {
            myListsCtrl.isHidden = true
            bottomSpaceNotificationContCons.priority = 750
        }
    }
    //MARK: - IBAction
    @IBAction func logout(){
        let alertCont = UIAlertController(title: "Alerta", message: "¿Desea cerrar sesión?", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Cerrar Sesión", style: .default, handler: { (action:UIAlertAction) -> Void in
            if User.deleteUser() {
                if self.presentingViewController == nil {
                    self.present(self.storyboard!.instantiateInitialViewController()!, animated: true, completion: nil)
                }else{
                    self.parent!.dismiss(animated: true, completion: nil)
                }
            }
        }))
        self.present(alertCont, animated: true, completion: nil)
    }
    @IBAction func dismissMenuViewCont(){
        if let mainViewCont = self.parent as? MainViewController {
            mainViewCont.hideMenu(0.5)
        }
    }
    @IBAction func showProfile(){
        self.changeViewCont(0)
    }
    @IBAction func showEvents(){
        self.changeViewCont(1)
    }
    @IBAction func showMyEvents(){
        self.changeViewCont(2)
    }
    @IBAction func showMyTickets(){
        self.changeViewCont(3)
    }
    @IBAction func showNotifications(){
        self.changeViewCont(4)
    }
    @IBAction func showMyLists(){
        self.changeViewCont(5)
    }
    func changeViewCont(_ newIndex:Int){
        if let mainViewCont = self.parent as? MainViewController {
            if mainViewCont.currentIndex != newIndex {
                mainViewCont.currentIndex = newIndex
            }
            mainViewCont.hideMenu(0.5)
        }
    }
}
