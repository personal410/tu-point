
//
//  MapViewController.swift
//  Tu Point
//
//  Created by victor salazar on 22/04/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import GoogleMaps
class MapViewController:CustomViewController{
    //MARK: - Variables
    var event:Event!
    //MARK: - IBOutlet
    @IBOutlet weak var eventNameLbl:UILabel!
    @IBOutlet weak var discoNameLbl:UILabel!
    @IBOutlet weak var mapView:GMSMapView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.eventNameLbl.text = self.event.name
        self.discoNameLbl.text = self.event.disco.name
        let camera = GMSCameraPosition.camera(withTarget: CLLocationCoordinate2D(latitude: self.event.disco.latitude.doubleValue, longitude: self.event.disco.longitude.doubleValue), zoom: 12)
        self.mapView.camera = camera
        let marker = GMSMarker(position: CLLocationCoordinate2D(latitude: self.event.disco.latitude.doubleValue, longitude: self.event.disco.longitude.doubleValue))
        marker.map = self.mapView
    }
}
