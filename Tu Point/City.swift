//
//  City.swift
//  Tu Point
//
//  Created by victor salazar on 7/04/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class City: NSManagedObject{
    class func createCities(_ arrCities:Array<Dictionary<String, AnyObject>>) -> Bool {
        let ctxt = AppDelegate.getMoc()
        for dicCity in arrCities {
            let newCity = NSEntityDescription.insertNewObject(forEntityName: "City", into: ctxt) as! City
            newCity.id = NSNumber(integerLiteral: (dicCity["id"] as! NSString).integerValue)
            newCity.name = dicCity["name"] as! String
        }
        do{
            try ctxt.save()
            return true
        }catch let error as NSError {
            print("createCities error: \(error.description)")
            return false
        }
    }
    class func getCityById(_ id:Int) -> City? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<City>(entityName: "City")
        fetchReq.predicate = NSPredicate(format: "id = %i", id)
        do{
            let results = try ctxt.fetch(fetchReq)
            return results.first
        }catch let error as NSError {
            print("getEventById error: \(error.description)")
            return nil
        }
    }
    class func getCities() -> Array<City> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<City>(entityName: "City")
        fetchReq.sortDescriptors = [NSSortDescriptor(key: "id", ascending: true)]
        do{
            let results = try ctxt.fetch(fetchReq)
            return results
        }catch let error as NSError {
            print("getEventById error: \(error.description)")
            return []
        }
    }
}
