//
//  CreateListViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 3/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CreateListViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate {
    //MARK: - Variables
    var arrCurrentSelectedIndexContacts:Array<Int> = []
    var promoterId = 0
    var eventId = 0
    var currentOption = 0
    var arrDicUsers:Array<Dictionary<String, AnyObject>> = []
    var arrDicFriends:Array<Dictionary<String, AnyObject>> = []
    var arrCurrentContacts = Array<Dictionary<String, AnyObject>>()
    //MARK: - IBOutlet
    @IBOutlet weak var sendListBtn:UIButton!
    @IBOutlet weak var searchTxtFld:UITextField!
    @IBOutlet weak var numberContactsLbl:UILabel!
    @IBOutlet weak var contactsTableView:UITableView!
    @IBOutlet weak var allUsersBtn:UIButton!
    @IBOutlet weak var friendsBtn:UIButton!
    @IBOutlet weak var selectedOptionView:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.sendListBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(self.sendListBtn.frame.size), at: 0)
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(CreateListViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateListViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.arrCurrentContacts = self.arrDicUsers
        if self.arrDicFriends.count == 0 && self.arrDicUsers.count == 0 {
            loadUsersAndFriends()
        }
    }
    //MARK: - IBAction
    @IBAction func addContact(_ btn:UIButton){
        for currentIndex in arrCurrentSelectedIndexContacts {
            if currentIndex == btn.tag {
                self.arrCurrentSelectedIndexContacts.remove(at: self.arrCurrentSelectedIndexContacts.index(of: currentIndex)!)
                self.contactsTableView.reloadData()
                self.updateNumberContactsLbl()
                return
            }
        }
        self.arrCurrentSelectedIndexContacts.append(btn.tag)
        self.contactsTableView.reloadData()
        self.updateNumberContactsLbl()
    }
    @IBAction func sendList(){
        if self.arrCurrentSelectedIndexContacts.count > 0 {
            let dicParams = ["action": "setList", "event_id": "\(eventId)", "users": self.arrCurrentSelectedIndexContacts.map(){"\($0)"}.joined(separator: ","), "user_id": "\(User.getUser()!.id.intValue)", "promoter_id": "\(promoterId)"]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
                SVProgressHUD.dismiss()
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                User.addEventIdInEventLists(self.eventId)
                                _ = self.navigationController?.popToViewController(self.navigationController!.viewControllers[1], animated: true)
                                return
                            }else{
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            }
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }else{
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Debe seleccionar como mínimo un contacto.", inViewCont: self)
        }
    }
    @IBAction func dismissKeyboard(){
        self.searchTxtFld.resignFirstResponder()
    }
    @IBAction func changeUsersList(btn:UIButton){
        if currentOption == btn.tag {
            return
        }
        searchTxtFld.text = ""
        currentOption = btn.tag
        if currentOption == 0 {
            self.arrCurrentContacts = self.arrDicUsers
            contactsTableView.reloadData()
            if selectedOptionView.transform.tx != 0 {
                UIView.animate(withDuration: 0.25, animations: {
                    self.selectedOptionView.transform = CGAffineTransform.init(translationX: 0, y: 0)
                })
            }
        }else{
            self.arrCurrentContacts = self.arrDicFriends
            contactsTableView.reloadData()
            if selectedOptionView.transform.tx == 0 {
                UIView.animate(withDuration: 0.25, animations: {
                    self.selectedOptionView.transform = CGAffineTransform.init(translationX: self.selectedOptionView.frame.size.width, y: 0)
                })
            }
        }
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrCurrentContacts.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        let dicContact = self.arrCurrentContacts[(indexPath as NSIndexPath).row]
        let id = (dicContact["id"] as! NSString).integerValue
        let imgName = self.arrCurrentSelectedIndexContacts.index(of: id) == nil ? "add" : "added"
        cell.addButton.setImage(UIImage(named: imgName), for: UIControlState())
        cell.contactImgView.image = nil
        cell.contactImgView.strUrl = dicContact["photo_url"] as! String
        cell.contactNameLbl.text = dicContact["name"] as? String
        cell.addButton.tag = id
        return cell
    }
    //MARK: - Auxiliar
    func updateNumberContactsLbl(){
        let numberFormattet = NumberFormatter()
        numberFormattet.minimumIntegerDigits = 2
        let number = NSNumber(value: self.arrCurrentSelectedIndexContacts.count as Int)
        self.numberContactsLbl.text = "\(numberFormattet.string(from: number)!) amigos añadidos"
    }
    func loadUsersAndFriends(){
        let dicParams = ["action": "getEventPromotersAndFriends", "event_id": "\(eventId)", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicFriendsTemp = dicResult["friends"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicFriends = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicFriendsTemp)
                            }
                            if let arrDicUsersTemp = dicResult["users"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicUsers = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicUsersTemp)
                            }
                            self.arrCurrentContacts = (self.currentOption == 0) ? self.arrDicUsers : self.arrDicFriends
                            SVProgressHUD.dismiss()
                            self.contactsTableView.reloadData()
                            return
                        }else{
                            SVProgressHUD.dismiss()
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            SVProgressHUD.dismiss()
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let totalHeight = self.view.frame.height
        let maxY = self.contactsTableView.frame.maxY
        let bottomSpace = totalHeight - maxY
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight - bottomSpace, right: 0)
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.characters.count == 0 {
            self.arrCurrentContacts = (currentOption == 0) ? self.arrDicUsers : self.arrDicFriends
        }else{
            self.arrCurrentContacts = ((currentOption == 0) ? self.arrDicUsers : self.arrDicFriends).filter(){($0["name"] as! String).range(of: newString, options: .caseInsensitive, range: nil, locale: nil) != nil}
        }
        self.contactsTableView.reloadData()
        return true
    }
}
