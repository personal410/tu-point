//
//  User.swift
//  Tu Point
//
//  Created by Victor Salazar on 23/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class User:NSManagedObject{
    override var description:String {
        return "<age: \(age), civilStatus: \(civil_status), cityId: \(city_id), dataPublic: \(data_public), email: \(email), fbId: \(fb_id), id: \(id), name: \(name), parseId: \(parse_id), phone: \(phone), photoUrl: \(photo_url), sex: \(sex), type: \(type)>"
    }
    class func createUser(_ dicUser:Dictionary<String, AnyObject>, isFriend:Bool = true) -> Bool {
        let ctxt = AppDelegate.getMoc()
        let newUser = NSEntityDescription.insertNewObject(forEntityName: "User", into: ctxt) as! User
        var age:NSNumber? = nil
        if let ageTemp = dicUser["age"] as? NSString {
            age = ageTemp.integerValue as NSNumber?
        }
        newUser.age = age
        newUser.civil_status = dicUser["civil_status"] as! String
        var city_id:NSNumber? = nil
        if let cityIdTemp = dicUser["city_id"] as? NSString {
            city_id = cityIdTemp.integerValue as NSNumber?
        }
        newUser.city_id = city_id
        var data_public = true
        if let strDataPublic = dicUser["data_public"] as? NSString {
            data_public = strDataPublic.boolValue
        }else if let bDataPublic = dicUser["data_public"] as? Bool {
            data_public = bDataPublic
        }
        newUser.data_public = data_public as NSNumber
        newUser.email = dicUser["email"] as! String
        var fb_id:NSNumber? = nil
        if let fbIdTemp = dicUser["fb_id"] as? NSString {
            fb_id = fbIdTemp.integerValue as NSNumber?
        }
        newUser.fb_id = fb_id
        newUser.id = NSNumber(integerLiteral: (dicUser["id"] as! NSString).integerValue)
        newUser.me = !isFriend as NSNumber
        newUser.name = dicUser["name"] as! String
        newUser.parse_id = dicUser["parse_id"] as? String
        newUser.phone = dicUser["phone"] as? String
        newUser.photo_url = dicUser["photo_url"] as? String
        newUser.sex = dicUser["sex"] as! String
        newUser.type = dicUser["type"] as! String
        newUser.in_events = dicUser["in_events"] as? String
        newUser.event_lists = dicUser["event_lists"] as? String
        if let preferences = dicUser["preferences"] as? String {
            newUser.preferences = preferences
        }else{
            newUser.preferences = ""
        }
        if let arrDiscos = dicUser["discos"] as? Array<Dictionary<String, AnyObject>> {
            if !Disco.createDiscos(arrDiscos) {
                ctxt.rollback()
                return false
            }
        }
        if let arrFriends = dicUser["friends"] as? Array<Dictionary<String, AnyObject>> {
            for dicFriend in arrFriends {
                _ = self.createUser(dicFriend)
            }
        }
        do{
            try ctxt.save()
            return true
        }catch let error as NSError {
            print("createUser error: \(error.description)")
            return false
        }
    }
    class func getUser() -> User? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<User>(entityName: "User")
        fetchReq.predicate = NSPredicate(format: "me == 1")
        do{
            let results = try ctxt.fetch(fetchReq)
            if results.count == 0 {
                return nil
            }else{
                return results.first
            }
        }catch let error as NSError {
            print("getUser error: \(error.description)")
            return nil
        }
    }
    class func addEventIdInInEvents(_ id:Int){
        if let user = self.getUser() {
            var arrInEvents = user.in_events!.components(separatedBy: ",").map(){($0 as NSString).integerValue}
            arrInEvents.append(id)
            let arrInEventsFinal = arrInEvents.map(){"\($0)"}
            user.in_events = arrInEventsFinal.joined(separator: ",")
            let ctxt = AppDelegate.getMoc()
            do{
                try ctxt.save()
            }catch let error as NSError {
                print("addEventIdInInEvents error: \(error.description)")
            }
        }
    }
    class func addEventIdInEventLists(_ id:Int){
        if let user = self.getUser() {
            var arrInEvents = user.event_lists!.components(separatedBy: ",").map(){($0 as NSString).integerValue}
            arrInEvents.append(id)
            let arrInEventsFinal = arrInEvents.map(){"\($0)"}
            user.event_lists = arrInEventsFinal.joined(separator: ",")
            let ctxt = AppDelegate.getMoc()
            do{
                try ctxt.save()
            }catch let error as NSError {
                print("addEventIdInEventLists error: \(error.description)")
            }
        }
    }
    class func getFriends() -> Array<User> {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<User>(entityName: "User")
        fetchReq.predicate = NSPredicate(format: "me == 0")
        do{
            let results = try ctxt.fetch(fetchReq)
            return results
        }catch let error as NSError {
            print("getFriends error: \(error.description)")
            return []
        }
    }
    class func deleteUser() -> Bool {
        if let user = self.getUser() {
            let ctxt = AppDelegate.getMoc()
            ctxt.delete(user)
            let arrDiscos = Disco.getDiscos()!
            for disco in arrDiscos {
                ctxt.delete(disco)
            }
            let arrEvents = Event.getEvents()!
            for event in arrEvents {
                ctxt.delete(event)
            }
            let arrCities = City.getCities()
            for city in arrCities {
                ctxt.delete(city)
            }
            let arrFriends = self.getFriends()
            for friend in arrFriends {
                ctxt.delete(friend)
            }
            let imagesDirec = ToolBox.getImagesDirectory()
            let fileManager = FileManager.default
            do{
                let filePaths = try fileManager.contentsOfDirectory(atPath: imagesDirec)
                for filePath in filePaths {
                    try fileManager.removeItem(atPath: "\(imagesDirec)/\(filePath)")
                }
            }catch let error as NSError {
                print("delete file error: \(error.description)")
            }
            do{
                try ctxt.save()
                return true
            }catch let error as NSError {
                print("deleteUser error: \(error.description)")
                return false
            }
        }else{
            return true
        }
    }
    class func updateUser(_ dicUser:Dictionary<String, String>){
        if let user = self.getUser() {
            let ctxt = AppDelegate.getMoc()
            user.phone = dicUser["phone"]
            user.email = dicUser["email"]!
            let age = dicUser["age"]!
            if age.characters.count == 0 {
                user.age = nil
            }else{
                user.age = (age as NSString).integerValue as NSNumber?
            }
            user.civil_status = dicUser["civil_status"]!
            user.sex = dicUser["sex"]!
            user.preferences = dicUser["preferences"]!
            let cityId = dicUser["city_id"]!
            if cityId.characters.count == 0 {
                user.city_id = nil
            }else{
                user.city_id = (cityId as NSString).integerValue as NSNumber?
            }
            do{
                try ctxt.save()
            }catch let error as NSError {
                print("updateUser error: \(error.description)")
            }
        }
    }
}
