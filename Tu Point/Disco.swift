//
//  Disco.swift
//  Tu Point
//
//  Created by Victor Salazar on 23/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class Disco:NSManagedObject{
    override var description:String {
        return "<id: \(id), address: \(address), name: \(name), cityId: \(city_id), phone: \(phone), latitude: \(latitude), longitude: \(longitude)>"
    }
    class func createDiscos(_ arrDiscos:Array<Dictionary<String, AnyObject>>) -> Bool {
        let ctxt = AppDelegate.getMoc()
        for dicDisco in arrDiscos {
            let newDisco = NSEntityDescription.insertNewObject(forEntityName: "Disco", into: ctxt) as! Disco
            newDisco.id = NSNumber(integerLiteral: (dicDisco["id"] as! NSString).integerValue)
            newDisco.address = dicDisco["address"] as! String
            newDisco.name = dicDisco["name"] as! String
            newDisco.city_id = NSNumber(integerLiteral: (dicDisco["city_id"] as! NSString).integerValue)
            newDisco.phone = dicDisco["phone"] as! String
            newDisco.latitude = NSNumber(floatLiteral: (dicDisco["latitude"] as! NSString).doubleValue)
            newDisco.longitude = NSNumber(floatLiteral: (dicDisco["longitude"] as! NSString).doubleValue)
        }
        do{
            try ctxt.save()
            return true
        }catch let error as NSError {
            print("createDiscos error: \(error.description)")
            return false
        }
    }
    class func getDiscos() -> Array<Disco>? {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<Disco>(entityName: "Disco")
        do{
            let results = try ctxt.fetch(fetchReq) 
            return results
        }catch let error as NSError {
            print("getDiscos error: \(error.description)")
            return nil
        }
    }
}
