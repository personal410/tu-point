//
//  AdminCreateEventViewController.swift
//  Tu Point
//
//  Created by victor salazar on 18/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import AVFoundation
class AdminCreateEventViewController:CustomViewController, UITextFieldDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: - Variables
    let calendar = Calendar.current
    let arrDiscos = Disco.getDiscos()!
    let arrEventTypes:Array<Dictionary<String, String>> = [["code": "free", "name": "Público"], ["code": "list", "name": "Lista"], ["code": "exclusive", "name": "Exclusiva"]]
    let arrOptionsNewPhoto = ["Tomar fotografía", "Subir desde el carrete", "Cancelar"]
    var event:Event?
    var currentEventType:Int = 0 {
        didSet{
            typeTxtFld.text = arrEventTypes[currentEventType]["name"]
            publicPromotersViewHeight.constant = currentEventType == 1 ? 38 : 0
        }
    }
    var currentSequenceDate = 0
    var isStartDate:Bool = false
    var startDate = Date()
    var endDate = Date()
    var currentList = 0
    var originalImg:UIImage?
    var finalImage:UIImage?
    var currentDisco = 0
    var arrDicPromoters:Array<Dictionary<String, AnyObject>> = []
    var arrDicUsers:Array<Dictionary<String, AnyObject>> = []
    //MARK: - IBOutlet
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var mainsScrollView:UIScrollView!
    @IBOutlet weak var eventNameTxtFld:UITextField!
    @IBOutlet weak var eventImgView:LoadingImageView!
    @IBOutlet weak var localTxtFld:UITextField!
    @IBOutlet weak var typeTxtFld:UITextField!
    @IBOutlet weak var startDateTxtFld:UITextField!
    @IBOutlet weak var endDateTxtFld:UITextField!
    @IBOutlet weak var publicPromotersViewHeight:NSLayoutConstraint!
    @IBOutlet weak var publicPromotersSwtch:UISwitch!
    @IBOutlet weak var descriptionTxtView:UITextView!
    @IBOutlet weak var createEventBtn:UIButton!
    @IBOutlet weak var layerOverImgView:UIView!
    @IBOutlet weak var listGradientView:UIView!
    @IBOutlet weak var optionListTableView:UITableView!
    @IBOutlet weak var heightOptionListTableViewCons:NSLayoutConstraint!
    @IBOutlet weak var datesView:UIView!
    @IBOutlet weak var currentDateTitleLbl:UILabel!
    @IBOutlet weak var firstDatePicker:UIDatePicker!
    @IBOutlet weak var secondDatePicker:UIDatePicker!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.currentEventType = 0
        self.localTxtFld.text = self.arrDiscos.first!.name
        if event != nil {
            self.titleLbl.text = "EDITA EL EVENTO"
            self.createEventBtn.setTitle("GUARDAR CAMBIOS", for: UIControlState())
            self.layerOverImgView.isHidden = false
            self.eventImgView.strUrl = event!.photo_url
            self.eventNameTxtFld.text = event?.name
            currentDisco = arrDiscos.index(of: event!.disco)!
            currentEventType = arrEventTypes.map(){$0["code"]!}.index(of: event!.type)!
            startDate = event!.date_time_start as Date
            endDate = event!.date_time_finish as Date
            self.descriptionTxtView.text = event!.eveDescription
        }
        self.localTxtFld.text = self.arrDiscos[currentDisco].name
        self.eventNameTxtFld.attributedPlaceholder = NSAttributedString(string: "Nombre del evento", attributes: [NSForegroundColorAttributeName: UIColor(r: 129, g: 130, b: 131)])
        self.descriptionTxtView.textContainer.lineFragmentPadding = 0
        self.descriptionTxtView.textContainerInset = UIEdgeInsets.zero
        let btnSize = CGSize(width: self.view.frame.width - 40, height: 40)
        self.createEventBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(btnSize, withCornerRadius: 10), at: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(AdminCreateEventViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AdminCreateEventViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        let colorTop = UIColor.clear.cgColor
        let colorMiddle = UIColor(rgb: 0, a: 0.95).cgColor
        let colorBottom = UIColor(rgb: 0, a: 0.95).cgColor
        let listGradientLayer = CAGradientLayer()
        listGradientLayer.frame.origin = CGPoint.zero
        listGradientLayer.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height - 114)
        listGradientLayer.colors = [colorTop, colorMiddle, colorBottom]
        listGradientLayer.locations = [0, 0.2, 1]
        self.listGradientView.layer.insertSublayer(listGradientLayer, at: 0)
        
        self.firstDatePicker.setValue(UIColor.white, forKey: "textColor")
        self.firstDatePicker.datePickerMode = .countDownTimer
        self.firstDatePicker.datePickerMode = .date
        self.firstDatePicker.minimumDate = Date()
        self.secondDatePicker.setValue(UIColor.white, forKey: "textColor")
        self.startDateTxtFld.text = ToolBox.convertDateToString(startDate)
        self.endDateTxtFld.text = ToolBox.convertDateToString(endDate)
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        if let finalImg = finalImage {
            self.eventImgView.image = finalImg
            self.layerOverImgView.isHidden = false
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? AdminCropPhotoViewController {
            viewCont.originalImage = self.originalImg!
        }else if let viewCont = segue.destination as? AdminAssignPromoterViewController {
            viewCont.event = self.event!
            viewCont.arrDicPromoters = self.arrDicPromoters
            viewCont.arrDicUsers = self.arrDicUsers
        }
    }
    //MARK: - IBAction
    @IBAction func showAssignPromoters(){
        if event != nil {
            if eventNameTxtFld.text!.characters.count == 0 || self.descriptionTxtView.text.characters.count == 0 {
                ToolBox.showAlertWithTitle("Alerta", withMessage: "Todos los campos son requeridos.", inViewCont: self)
            }else{
                var dicParams:Dictionary<String, String> = ["action": "editEvent", "key": URLs.key, "event_id": "\(event!.id.intValue)", "disco_id" : "\(self.arrDiscos[currentDisco].id.intValue)", "name": self.eventNameTxtFld.text!, "description": self.descriptionTxtView.text, "type" : arrEventTypes[currentEventType]["code"]!, "date_time_start": ToolBox.convertDateToString(startDate), "date_time_finish": ToolBox.convertDateToString(endDate), "public_promoters" : "\(self.publicPromotersSwtch.isSelected ? 1 : 0)"]
                if let finalImage = finalImage {
                    let imgData = UIImagePNGRepresentation(finalImage)!
                    let strImgData = imgData.base64EncodedString(options: [])
                    dicParams["photo"] = strImgData
                }
                var strParams = ToolBox.convertDictionaryToGetParams(dicParams)
                strParams = (strParams as NSString).replacingOccurrences(of: "+", with: "%2B") as String
                SVProgressHUD.show(withStatus: "Cargando")
                ServiceConnector.connectToUrl(URLs.rootPostURL, params: strParams as AnyObject?, response:{(result:AnyObject?, error:NSError?) -> Void in
                    SVProgressHUD.dismiss()
                    if error == nil {
                        if let dicResult = result as? Dictionary<String, AnyObject> {
                            if let state = dicResult["state"] as? String {
                                if state == "success" {
                                    if let dicEventTemp = dicResult["event"] as? Dictionary<String, AnyObject> {
                                        if Event.editEvent(dicEventTemp) {
                                            _ = self.navigationController?.popViewController(animated: true)
                                        }
                                    }
                                }else{
                                    let message = dicResult["message"] as! String
                                    ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                }
                                return
                            }
                        }
                    }
                    ToolBox.showErrorConnectionInViewCont(self)
                })
            }
        }else{
            if eventNameTxtFld.text!.characters.count == 0 || self.finalImage == nil || self.descriptionTxtView.text.characters.count == 0 {
                ToolBox.showAlertWithTitle("Alerta", withMessage: "Todos los campos son requeridos.", inViewCont: self)
            }else{
                let imgData = UIImagePNGRepresentation(self.finalImage!)!
                let strImgData = imgData.base64EncodedString(options: [])
                let dicParams:Dictionary<String, String> = ["action": "createEvent", "key": URLs.key, "disco_id" : "\(self.arrDiscos[currentDisco].id.intValue)", "name": self.eventNameTxtFld.text!, "description": self.descriptionTxtView.text, "type" : arrEventTypes[currentEventType]["code"]!, "date_time_start": ToolBox.convertDateToString(startDate), "date_time_finish": ToolBox.convertDateToString(endDate), "public_promoters" : "\(self.publicPromotersSwtch.isOn ? 1 : 0)", "photo": strImgData]
                var strParams = ToolBox.convertDictionaryToGetParams(dicParams)
                strParams = (strParams as NSString).replacingOccurrences(of: "+", with: "%2B") as String
                SVProgressHUD.show(withStatus: "Cargando")
                ServiceConnector.connectToUrl(URLs.rootPostURL, params: strParams as AnyObject?, response:{(result:AnyObject?, error:NSError?) -> Void in
                    if error == nil {
                        if let dicResult = result as? Dictionary<String, AnyObject> {
                            if let state = dicResult["state"] as? String {
                                if state == "success" {
                                    if let dicEventTemp = dicResult["event"] as? Dictionary<String, AnyObject> {
                                        if let newEvent = Event.createEvent(dicEventTemp) {
                                            self.event = newEvent
                                            self.arrDicPromoters = ToolBox.orderDicObjectsByName(arrDicObjects: dicResult["promoters"] as! Array<Dictionary<String, AnyObject>>)
                                            self.arrDicUsers = ToolBox.orderDicObjectsByName(arrDicObjects: dicResult["users"] as! Array<Dictionary<String, AnyObject>>)
                                            SVProgressHUD.dismiss()
                                            self.performSegue(withIdentifier: "showAssignPromoters", sender: nil)
                                            return
                                        }
                                    }
                                }else{
                                    SVProgressHUD.dismiss()
                                    let message = dicResult["message"] as! String
                                    ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                    return
                                }
                            }
                        }
                    }
                    SVProgressHUD.dismiss()
                    ToolBox.showErrorConnectionInViewCont(self)
                })
            }
        }
    }
    @IBAction func editText(_ ctrl:UIControl){
        if ctrl.tag == 0 {
            self.localTxtFld.becomeFirstResponder()
        }else if ctrl.tag == 1 {
            self.typeTxtFld.becomeFirstResponder()
        }else if ctrl.tag == 2 {
            self.startDateTxtFld.becomeFirstResponder()
        }else if ctrl.tag == 3 {
            self.endDateTxtFld.becomeFirstResponder()
        }
    }
    @IBAction func dissmissGradientView(){
        self.hideList()
    }
    @IBAction func dissmissKeyboard(){
        self.eventNameTxtFld.resignFirstResponder()
        self.descriptionTxtView.resignFirstResponder()
    }
    @IBAction func confirmDate(){
        if currentSequenceDate == 0 {
            UIView.animate(withDuration: 0.25, animations: {
                self.firstDatePicker.transform = CGAffineTransform(translationX: -self.firstDatePicker.frame.width, y: 0)
                self.secondDatePicker.transform = CGAffineTransform.identity
            })
            currentSequenceDate += 1
        }else{
            let firstDateComps = (Calendar.current as NSCalendar).components([.year, .month, .day], from: self.firstDatePicker.date)
            let secondDateComps = (Calendar.current as NSCalendar).components([.hour, .minute], from: self.secondDatePicker.date)
            if isStartDate {
                var startDateComps:DateComponents = DateComponents()
                startDateComps.day = firstDateComps.day
                startDateComps.month = firstDateComps.month
                startDateComps.year = firstDateComps.year
                startDateComps.hour = secondDateComps.hour
                startDateComps.minute = secondDateComps.minute
                startDate = calendar.date(from: startDateComps)!
                self.startDateTxtFld.text = ToolBox.convertDateToString(startDate)
            }else{
                var endDateComps:DateComponents = DateComponents()
                endDateComps.day = firstDateComps.day
                endDateComps.month = firstDateComps.month
                endDateComps.year = firstDateComps.year
                endDateComps.hour = secondDateComps.hour
                endDateComps.minute = secondDateComps.minute
                endDate = calendar.date(from: endDateComps)!
                self.endDateTxtFld.text = ToolBox.convertDateToString(endDate)
            }
            self.hideList()
        }
    }
    @IBAction func selectImage(){
        currentList = 1
        self.eventNameTxtFld.resignFirstResponder()
        self.datesView.isHidden = true
        self.optionListTableView.isHidden = false
        self.heightOptionListTableViewCons.constant = 180
        self.optionListTableView.reloadData()
        showList()
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0)
        self.mainsScrollView.contentInset = contentInset
        self.mainsScrollView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.mainsScrollView.contentInset = contentInset
        self.mainsScrollView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldBeginEditing(_ textField:UITextField) -> Bool {
        if textField.tag == 0 {
            return self.arrDiscos.count > 1
        }else if textField.tag == 1 {
            self.datesView.isHidden = true
            self.optionListTableView.isHidden = false
            currentList = 0
            self.heightOptionListTableViewCons.constant = 240
            self.showList()
            self.optionListTableView.reloadData()
            return false
        }else if textField.tag < 4 {
            self.datesView.isHidden = false
            self.optionListTableView.isHidden = true
            self.currentSequenceDate = 0
            self.isStartDate = textField.tag == 2
            self.currentDateTitleLbl.text = (textField.tag == 2) ? "Fecha de inicio" : "Fecha de fin"
            self.firstDatePicker.transform = CGAffineTransform.identity
            self.secondDatePicker.transform = CGAffineTransform(translationX: self.secondDatePicker.frame.width, y: 0)
            if textField.tag == 2 {
                self.firstDatePicker.date = startDate
                self.secondDatePicker.date = startDate
            }else{
                self.firstDatePicker.date = endDate
                self.secondDatePicker.date = endDate
            }
            self.showList()
            return false
        }
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        if textField.tag == 5 {
            let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
            return newString.characters.count < 255
        }
        return true
    }
    //MARK: - Auxiliar
    func showList(){
        self.listGradientView.isHidden = false
        self.listGradientView.transform = CGAffineTransform(translationX: 0, y: self.listGradientView.frame.height)
        UIView.animate(withDuration: 0.25, animations: {
            self.listGradientView.transform = CGAffineTransform.identity
        })
    }
    func hideList(){
        UIView.animate(withDuration: 0.25, animations: {
            self.listGradientView.transform = CGAffineTransform(translationX: 0, y: self.listGradientView.frame.height)
        }, completion: {(b:Bool) -> Void in
            self.listGradientView.isHidden = false
        })
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrEventTypes.count
    }
    func tableView(_ tableView:UITableView, cellForRowAtIndexPath indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "optionCell", for: indexPath) as! ListOptionTableViewCell
        if currentList == 0 {
            cell.titleLbl.text = self.arrEventTypes[(indexPath as NSIndexPath).row]["name"]
        }else{
            cell.titleLbl.text = self.arrOptionsNewPhoto[(indexPath as NSIndexPath).row]
        }
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAtIndexPath indexPath:IndexPath){
        if currentList == 0 {
            self.currentEventType = (indexPath as NSIndexPath).row
            self.hideList()
        }else{
            if (indexPath as NSIndexPath).row == 2 {
                self.hideList()
            }else{
                let sourceType:UIImagePickerControllerSourceType = (indexPath.row == 0) ? .camera : .photoLibrary
                if UIImagePickerController.isSourceTypeAvailable(sourceType) {
                    if sourceType == .camera {
                        let status = AVCaptureDevice.authorizationStatus(forMediaType: AVMediaTypeVideo)
                        if status == .denied || status == . restricted {
                            ToolBox.showAlertWithTitle("Error", withMessage: "Por favor, habilitado el uso de la camara desde ajuste para habilitar esta opción.", inViewCont: self)
                            return
                        }
                    }
                    let imagePickerViewCont = UIImagePickerController()
                    imagePickerViewCont.sourceType = sourceType
                    imagePickerViewCont.delegate = self
                    imagePickerViewCont.mediaTypes = ["public.image"]
                    self.hideList()
                    UIApplication.shared.statusBarStyle = .default
                    self.present(imagePickerViewCont, animated: true, completion: nil)
                }else{
                    let message = ((indexPath as NSIndexPath).row == 0) ? "No se puede tomar foto ahora" : "No se puede acceder a su libreria de fotos"
                    ToolBox.showAlertWithTitle("Alerta", withMessage: message, inViewCont: self)
                }
            }
        }
    }
    //MARK: - ImagePicker
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let mediaType = info[UIImagePickerControllerMediaType] as! String
        self.dismiss(animated: true, completion: nil)
        if mediaType == "public.image" {
            self.originalImg = info[UIImagePickerControllerOriginalImage] as? UIImage
            self.performSegue(withIdentifier: "showCropPhoto", sender: nil)
        }
    }
    func imagePickerControllerDidCancel(_ picker:UIImagePickerController){
        self.dismiss(animated: true, completion: nil)
    }
}
