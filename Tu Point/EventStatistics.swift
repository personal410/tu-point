//
//  EventStatistics.swift
//  Tu Point
//
//  Created by Victor Salazar on 28/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class EventStatistics:NSManagedObject{
    class func createEventStatistics(_ dicEventStatistics:Dictionary<String, AnyObject>) -> EventStatistics {
        let ctxt = AppDelegate.getMoc()
        let newEventStatistics = NSEntityDescription.insertNewObject(forEntityName: "EventStatistics", into: ctxt) as! EventStatistics
        newEventStatistics.assistants = NSNumber(integerLiteral: (dicEventStatistics["assistants"] as! NSString).integerValue)
        newEventStatistics.attend = NSNumber(integerLiteral: (dicEventStatistics["attend"] as! NSString).integerValue)
        newEventStatistics.committed = dicEventStatistics["committed"] as! String
        var id:Int = 0;
        if let numberId = dicEventStatistics["id"] as? NSNumber {
            id = numberId.intValue
        }else if let stringId = dicEventStatistics["id"] as? NSString {
            id = stringId.integerValue
        }
        newEventStatistics.id = NSNumber(integerLiteral: id)
        newEventStatistics.in_relationship = dicEventStatistics["in_relationship"] as! String
        newEventStatistics.married = dicEventStatistics["married"] as! String
        newEventStatistics.men = dicEventStatistics["men"] as! String
        newEventStatistics.open_relation = dicEventStatistics["open_relation"] as! String
        newEventStatistics.single = dicEventStatistics["single"] as! String
        newEventStatistics.undefined = dicEventStatistics["undefined"] as! String
        newEventStatistics.undefined_relation = dicEventStatistics["undefined_relation"] as! String
        newEventStatistics.women = dicEventStatistics["women"] as! String
        return newEventStatistics
    }
    class func getEventStatisticsById(_ id:Int) -> EventStatistics {
        let ctxt = AppDelegate.getMoc()
        let fetchReq = NSFetchRequest<EventStatistics>(entityName: "EventStatistics")
        fetchReq.predicate = NSPredicate(format: "id = %i", id)
        let results = try! ctxt.fetch(fetchReq)
        return results.first!
    }
    class func updateEventStatitics(_ dicEventStatistics:Dictionary<String, AnyObject>, eventStatistics:EventStatistics){
        eventStatistics.assistants = NSNumber(integerLiteral: (dicEventStatistics["assistants"] as! NSString).integerValue)
        eventStatistics.attend = NSNumber(integerLiteral: (dicEventStatistics["attend"] as! NSString).integerValue)
        eventStatistics.committed = dicEventStatistics["committed"] as! String
        eventStatistics.in_relationship = dicEventStatistics["in_relationship"] as! String
        eventStatistics.married = dicEventStatistics["married"] as! String
        eventStatistics.men = dicEventStatistics["men"] as! String
        eventStatistics.open_relation = dicEventStatistics["open_relation"] as! String
        eventStatistics.single = dicEventStatistics["single"] as! String
        eventStatistics.undefined = dicEventStatistics["undefined"] as! String
        eventStatistics.undefined_relation = dicEventStatistics["undefined_relation"] as! String
        eventStatistics.women = dicEventStatistics["women"] as! String
    }
}
