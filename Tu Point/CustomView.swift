//
//  CustomView.swift
//  Tu Point
//
//  Created by Victor Salazar on 2/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CustomView:UIView{
    var borderColor:UIColor! = UIColor.clear {
        didSet{
            self.layer.borderWidth = 1
            self.layer.borderColor = borderColor.cgColor
        }
    }
}
