//
//  ProfileViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 23/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ProfileViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UITextFieldDelegate{
    //MARK: - Variables
    let arrTitles = ["Teléfono", "Correo", "Edad", "Estado Civil", "Sexo", "Ciudad"]
    let arrMusicalGenres = ["Electrónica", "Rock", "Reggeatón", "Pachanga", "Salsa", "Cumbia", "Otros"]
    let arrFavDrinks = ["Agua", "Cerveza", "Jager", "Pisco", "Ron", "Whisky", "Otros"]
    let arrReasons = ["Buscar pareja", "Conocer gente", "Grupo de amigos", "Solo divertirme"]
    
    var arrLikesMusicalGenres = [0, 0, 0, 0, 0, 0, 0]
    var arrLikesFavDrinks = [0, 0, 0, 0, 0, 0, 0]
    var arrLikesReasons = [0, 0, 0, 0]
    
    let arrDicCivilStatus = ToolBox.getCivilStatuses()
    let arrDicGender = ToolBox.getGenders()
    var currentTxtFld:UITextField? = nil
    var currentList = 0 {
        didSet{
            self.listTableView.reloadData()
        }
    }
    let arrCities = City.getCities()
    var currentCivilStatus = -1
    var currentGender = -1
    var currentCity = -1
    let user = User.getUser()!
    var phone = ""
    var email = ""
    var age = ""
    //MARK: - IBOutlet
    @IBOutlet weak var profileScrollView:UIScrollView!
    @IBOutlet weak var profileImgView:UIImageView!
    @IBOutlet weak var profileNameLbl:UILabel!
    @IBOutlet weak var profileIDLbl:UILabel!
    @IBOutlet weak var moreInfoLbl:UILabel!
    @IBOutlet weak var profileTableView:UITableView!
    @IBOutlet weak var moreInfoScrollView:UIScrollView!
    @IBOutlet weak var musicalGenreCollectionView:UICollectionView!
    @IBOutlet weak var favDrinksCollectionView:UICollectionView!
    @IBOutlet weak var reasonCollectionView:UICollectionView!
    @IBOutlet weak var listView:UIView!
    @IBOutlet weak var listTableView:UITableView!
    @IBOutlet weak var saveChangesBtn:UIButton!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.profileImgView.layer.borderColor = UIColor.white.cgColor
        let attributtedText = NSAttributedString(string: "Ver más información", attributes: [NSUnderlineStyleAttributeName: NSUnderlineStyle.styleSingle.rawValue, NSFontAttributeName: UIFont(name: "MuseoSans-300", size: 12)!])
        self.moreInfoLbl.attributedText = attributtedText
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ProfileViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        if let fbId = user.fb_id {
            let filename = "\(fbId.intValue).png"
            let imageDirPath = ToolBox.getImagesDirectory()
            let imagePath = "\(imageDirPath)/\(filename)"
            self.profileImgView.image = UIImage(contentsOfFile: imagePath)
        }
        self.profileNameLbl.text = user.name
        self.profileIDLbl.text = "ID: \(user.id.intValue)"
        self.saveChangesBtn.layer.addSublayer(ToolBox.createBlueGradientLayerWithSize(CGSize(width: 200, height: 40)))
        let arrCodeCivilStatus = arrDicCivilStatus.map(){$0["code"]!}
        if let index = arrCodeCivilStatus.index(of: user.civil_status) {
            currentCivilStatus = index
        }
        let arrCodeGender = arrDicGender.map(){$0["code"]!}
        if let index = arrCodeGender.index(of: user.sex) {
            currentGender = index
        }
        if let cityId = user.city_id {
            let arrCityIds = self.arrCities.map(){$0.id.intValue}
            currentCity = arrCityIds.index(of: cityId.intValue)!
        }
        if let phoneTemp = user.phone {
            phone = phoneTemp
        }
        email = user.email
        if let ageTemp = user.age {
            age = "\(ageTemp.intValue)"
        }
        if self.user.preferences.characters.count > 0 {
            let arrPrefences = self.user.preferences.components(separatedBy: ",")
            for pref in arrPrefences {
                let preference = (pref as NSString).integerValue - 1
                if preference < 7 {
                    self.arrLikesMusicalGenres[preference] = 1
                }else if preference < 14 {
                    self.arrLikesFavDrinks[preference - 7] = 1
                }else{
                    self.arrLikesReasons[preference - 14] = 1
                }
            }
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        if tableView == profileTableView {
            return self.arrTitles.count
        }else{
            if currentList == 0 {
                return self.arrDicCivilStatus.count
            }else if currentList == 1 {
                return self.arrDicGender.count
            }else{
                return self.arrCities.count
            }
        }
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        if tableView == self.profileTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "infoCell", for: indexPath) as! InfoProfileTableViewCell
            cell.titleLbl.text = "\(self.arrTitles[(indexPath as NSIndexPath).row]):"
            if (indexPath as NSIndexPath).row > 0 {
                cell.valueTxtFld.textColor = UIColor.white
            }
            cell.valueTxtFld.tag = (indexPath as NSIndexPath).row
            if (indexPath as NSIndexPath).row == 0 {
                cell.valueTxtFld.keyboardType = .phonePad
                cell.valueTxtFld.text = phone
            }else if (indexPath as NSIndexPath).row == 1 {
                cell.valueTxtFld.keyboardType = .emailAddress
                cell.valueTxtFld.text = email
            }else if (indexPath as NSIndexPath).row == 2 {
                cell.valueTxtFld.keyboardType = .numberPad
                cell.valueTxtFld.text = age
            }else if (indexPath as NSIndexPath).row == 3 {
                cell.valueTxtFld.text = self.arrDicCivilStatus[currentCivilStatus]["title"]
            }else if (indexPath as NSIndexPath).row == 4 {
                cell.valueTxtFld.text = self.arrDicGender[currentGender]["title"]
            }else if (indexPath as NSIndexPath).row == 5 {
                if currentCity == -1 {
                    cell.valueTxtFld.text = ""
                }else{
                    cell.valueTxtFld.text = self.arrCities[self.currentCity].name
                }
            }
            cell.editCtrl.tag = (indexPath as NSIndexPath).row
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ItemListTableViewCell
            if self.currentList == 0 {
                cell.titleLbl.text = self.arrDicCivilStatus[(indexPath as NSIndexPath).row]["title"]
                cell.checkImgView.isHidden = !((indexPath as NSIndexPath).row == currentCivilStatus)
            }else if self.currentList == 1 {
                cell.titleLbl.text = self.arrDicGender[(indexPath as NSIndexPath).row]["title"]
                cell.checkImgView.isHidden = !((indexPath as NSIndexPath).row == currentGender)
            }else{
                cell.titleLbl.text = self.arrCities[(indexPath as NSIndexPath).row].name
                cell.checkImgView.isHidden = !((indexPath as NSIndexPath).row == currentCity)
            }
            return cell
        }
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        if tableView == listTableView {
            if currentList == 0 {
                if (indexPath as NSIndexPath).row != currentCivilStatus {
                    if let cell = listTableView.cellForRow(at: IndexPath(row: currentCivilStatus, section: 0)) as? ItemListTableViewCell {
                        cell.checkImgView.isHidden = true
                    }
                    currentCivilStatus = (indexPath as NSIndexPath).row
                    let cell = listTableView.cellForRow(at: IndexPath(row: currentCivilStatus, section: 0)) as! ItemListTableViewCell
                    cell.checkImgView.isHidden = false
                    let civilStatusCell = self.profileTableView.cellForRow(at: IndexPath(row: 3, section: 0)) as! InfoProfileTableViewCell
                    civilStatusCell.valueTxtFld.text = cell.titleLbl.text
                }
            }else if self.currentList == 1 {
                if let cell = listTableView.cellForRow(at: IndexPath(row: currentGender, section: 0)) as? ItemListTableViewCell {
                    cell.checkImgView.isHidden = true
                }
                currentGender = (indexPath as NSIndexPath).row
                let cell = listTableView.cellForRow(at: IndexPath(row: currentGender, section: 0)) as! ItemListTableViewCell
                cell.checkImgView.isHidden = false
                let civilStatusCell = self.profileTableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! InfoProfileTableViewCell
                civilStatusCell.valueTxtFld.text = cell.titleLbl.text
            }else{
                if let cell = listTableView.cellForRow(at: IndexPath(row: currentCity, section: 0)) as? ItemListTableViewCell {
                    cell.checkImgView.isHidden = true
                }
                currentCity = (indexPath as NSIndexPath).row
                let cell = listTableView.cellForRow(at: IndexPath(row: currentCity, section: 0)) as! ItemListTableViewCell
                cell.checkImgView.isHidden = false
                let civilStatusCell = self.profileTableView.cellForRow(at: IndexPath(row: 5, section: 0)) as! InfoProfileTableViewCell
                civilStatusCell.valueTxtFld.text = cell.titleLbl.text
            }
        }
    }
    //MARK: - CollectionView
    func collectionView(_ collectionView:UICollectionView, numberOfItemsInSection section:Int) -> Int {
        if collectionView == musicalGenreCollectionView {
            return arrMusicalGenres.count
        }else if collectionView == favDrinksCollectionView {
            return arrFavDrinks.count
        }else{
            return arrReasons.count
        }
    }
    func collectionView(_ collectionView:UICollectionView, cellForItemAt indexPath:IndexPath) -> UICollectionViewCell {
        if collectionView == musicalGenreCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "musicalGenreCell", for: indexPath) as! CheckItemCollectionViewCell
            cell.titleItemLbl.text = self.arrMusicalGenres[(indexPath as NSIndexPath).item]
            cell.itemCheckBoxView.checked = self.arrLikesMusicalGenres[(indexPath as NSIndexPath).row] == 1
            return cell
        }else if collectionView == favDrinksCollectionView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "favDrinkCell", for: indexPath) as! CheckItemCollectionViewCell
            cell.titleItemLbl.text = self.arrFavDrinks[(indexPath as NSIndexPath).item]
            cell.itemCheckBoxView.checked = self.arrLikesFavDrinks[(indexPath as NSIndexPath).row] == 1
            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "reasonCell", for: indexPath) as! CheckItemCollectionViewCell
            cell.titleItemLbl.text = self.arrReasons[(indexPath as NSIndexPath).item]
            cell.itemCheckBoxView.checked = self.arrLikesReasons[(indexPath as NSIndexPath).row] == 1
            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = floor(collectionView.frame.width / 2.0)
        return CGSize(width: width, height: 30)
    }
    func collectionView(_ collectionView:UICollectionView, didSelectItemAt indexPath:IndexPath){
        if collectionView == self.musicalGenreCollectionView {
            self.arrLikesMusicalGenres[(indexPath as NSIndexPath).row] = 1 - self.arrLikesMusicalGenres[(indexPath as NSIndexPath).row]
            self.musicalGenreCollectionView.reloadItems(at: [indexPath])
        }else if collectionView == self.favDrinksCollectionView {
            self.arrLikesFavDrinks[(indexPath as NSIndexPath).row] = 1 - self.arrLikesFavDrinks[(indexPath as NSIndexPath).row]
            self.favDrinksCollectionView.reloadItems(at: [indexPath])
        }else{
            self.arrLikesReasons[(indexPath as NSIndexPath).row] = 1 - self.arrLikesReasons[(indexPath as NSIndexPath).row]
            self.reasonCollectionView.reloadItems(at: [indexPath])
        }
    }
    //MARK: - IBAction
    @IBAction func showMoreInfo(){
        if self.moreInfoScrollView.isHidden {
            self.moreInfoScrollView.isHidden = false
            self.moreInfoScrollView.transform = CGAffineTransform(translationX: 0, y: self.moreInfoScrollView.frame.height)
            UIView.animate(withDuration: 0.25, animations: {
                self.moreInfoScrollView.transform = CGAffineTransform.identity
            })
        }
    }
    @IBAction func dissmissMoreInfo(){
        self.dissmissMoreInfoWithAnimation()
    }
    @IBAction func okMoreInfo(){
        self.dissmissMoreInfoWithAnimation()
    }
    @IBAction func okList(){
        if !listView.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.listView.transform = CGAffineTransform(translationX: 0, y: self.listView.frame.height)
                }, completion:{(b:Bool) in
                    self.profileScrollView.isScrollEnabled = true
                    self.listView.isHidden = true
                    self.listView.transform = CGAffineTransform.identity
            })
        }
    }
    @IBAction func dismissKeyboard(){
        self.currentTxtFld?.resignFirstResponder()
        if !listView.isHidden {
            UIView.animate(withDuration: 0.25, animations: {
                self.listView.transform = CGAffineTransform(translationX: 0, y: self.listView.frame.height)
            }, completion:{(b:Bool) in
                self.profileScrollView.isScrollEnabled = true
                self.listView.isHidden = true
                self.listView.transform = CGAffineTransform.identity
            })
        }
    }
    @IBAction func saveChanges(){
        currentTxtFld?.resignFirstResponder()
        var cityId = ""
        if currentCity != -1 {
            cityId = "\(self.arrCities[currentCity].id.intValue)"
        }
        var selectedPreferences:Array<Int> = []
        for i in 0 ..< self.arrLikesMusicalGenres.count {
            if self.arrLikesMusicalGenres[i] == 1 {
                selectedPreferences.append(i + 1)
            }
        }
        for i in 0 ..< self.arrLikesFavDrinks.count {
            if self.arrLikesFavDrinks[i] == 1 {
                selectedPreferences.append(i + 8)
            }
        }
        for i in 0 ..< self.arrLikesReasons.count {
            if self.arrLikesReasons[i] == 1 {
                selectedPreferences.append(i + 15)
            }
        }
        let dicParams = ["action": "updateProfile", "user_id": "\(self.user.id.intValue)", "email": email, "phone": phone, "age": age, "sex": self.arrDicGender[currentGender]["code"]!, "civil_status": self.arrDicCivilStatus[currentCivilStatus]["code"]!, "city_id": cityId, "preferences": selectedPreferences.map(){"\($0)"}.joined(separator: ",")]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            User.updateUser(dicParams)
                            ToolBox.showAlertWithTitle("Alerta", withMessage: "Se actualizo correctamente los datos del usuario", inViewCont: self)
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                        return
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    @IBAction func editTxtFld(_ ctrl:UIControl){
        let cell = self.profileTableView.cellForRow(at: IndexPath(row: ctrl.tag, section: 0)) as! InfoProfileTableViewCell
        cell.valueTxtFld.becomeFirstResponder()
    }
    //MARK: - Auxiliar
    func dissmissMoreInfoWithAnimation(){
        UIView.animate(withDuration: 0.25, animations: {
            self.moreInfoScrollView.transform = CGAffineTransform(translationX: 0, y: self.moreInfoScrollView.frame.height)
            }, completion: {(b:Bool) in
                self.moreInfoScrollView.transform = CGAffineTransform.identity
                self.moreInfoScrollView.isHidden = true
        })
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0)
        self.profileScrollView.contentInset = contentInset
        self.profileScrollView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.profileScrollView.contentInset = contentInset
        self.profileScrollView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldBeginEditing(_ textField:UITextField) -> Bool {
        if case 3 ... 5 = textField.tag {
            self.currentTxtFld?.resignFirstResponder()
            currentList = textField.tag - 3
            self.listView.isHidden = false
            self.profileScrollView.setContentOffset(CGPoint.zero, animated: true)
            self.profileScrollView.isScrollEnabled = false
            self.listView.transform = CGAffineTransform(translationX: 0, y: self.listView.frame.height)
            UIView.animate(withDuration: 0.25, animations: {
                self.listView.transform = CGAffineTransform.identity
            })
            return false
        }
        currentTxtFld = textField
        return true
    }
    func textFieldDidEndEditing(_ textField:UITextField){
        if textField.tag == 0 {
            phone = textField.text!
        }else if textField.tag == 1 {
            email = textField.text!
        }else if textField.tag == 2 {
            age = textField.text!
        }
    }
}
