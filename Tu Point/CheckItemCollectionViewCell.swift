//
//  CheckItemCollectionViewCell.swift
//  Tu Point
//
//  Created by victor salazar on 25/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CheckItemCollectionViewCell:UICollectionViewCell{
    @IBOutlet weak var itemCheckBoxView:CheckBoxView!
    @IBOutlet weak var titleItemLbl:UILabel!
}