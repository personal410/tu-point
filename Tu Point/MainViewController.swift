//
//  MainViewController.swift
//  Tu Point
//
//  Created by victor salazar on 20/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MainViewController:UIViewController, UIGestureRecognizerDelegate{
    //MARK: - Variables
    var arrViewConts = ["profileNavCont", "eventNavCont", "myEventsNavCont", "myTicketsNavCont", "notificationsNavCont", "myListsNavCont"]
    var initialPoint = CGPoint.zero
    var currentIndex:Int! {
        didSet{
            for childViewCont in self.childViewControllers {
                if !childViewCont.isKind(of: MenuViewController.self) {
                    childViewCont.removeFromParentViewController()
                }
            }
            for subView in self.contentView.subviews {
                subView.removeFromSuperview()
            }
            let currentViewCont = arrViewConts[currentIndex]
            let viewCont = self.storyboard!.instantiateViewController(withIdentifier: currentViewCont)
            var viewFrame = viewCont.view.frame
            viewFrame.size = self.contentView.frame.size
            viewCont.view.frame = viewFrame
            self.contentView.addSubview(viewCont.view)
            self.addChildViewController(viewCont)
        }
    }
    //MARK: - IBOutlet
    @IBOutlet weak var contentView:UIView!
    @IBOutlet weak var menuView:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        currentIndex = 1
        self.menuView.transform = CGAffineTransform(translationX: -self.view.frame.width, y: 0)
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    //MARK: - IBAction
    func showMenu(_ duration:Double){
        UIView.animate(withDuration: duration, animations: {
            self.menuView.transform = CGAffineTransform.identity
        })
    }
    @IBAction func panGesture(_ gesture:UIPanGestureRecognizer){
        if gesture.state == .began {
            initialPoint = gesture.location(in: self.view)
        }else if gesture.state == .changed {
            let currentPoint = gesture.location(in: self.view)
            var deltaX = currentPoint.x - initialPoint.x + self.menuView.transform.tx
            if self.menuView.transform.tx == -self.view.frame.width {
                deltaX += self.view.frame.size.width/4.0
            }
            if deltaX > 0 {
                deltaX = 0;
            }
            initialPoint = currentPoint
            self.menuView.transform = CGAffineTransform(translationX: deltaX, y: 0)
        }else if gesture.state == .ended || gesture.state == .failed || gesture.state == .cancelled {
            let tx = self.menuView.transform.tx
            if tx <= -(3.0*self.view.frame.size.width/8.0) {
                self.hideMenu(0.25)
            }else{
                self.showMenu(0.25)
            }
        }
    }
    //MARK: - Auxiliar
    func hideMenu(_ duration:Double){
        UIView.animate(withDuration: duration, animations: {
            self.menuView.transform = CGAffineTransform(translationX: -self.menuView.frame.width, y: 0)
        })
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer:UIGestureRecognizer) -> Bool {
        let point = gestureRecognizer.location(in: self.view)
        if self.menuView.isHidden {
            return point.x < 50
        }else{
            return true
        }
    }
}
