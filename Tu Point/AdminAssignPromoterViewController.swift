//
//  AdminAssignPromoterViewController.swift
//  Tu Point
//
//  Created by victor salazar on 19/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminAssignPromoterViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIScrollViewDelegate {
    //MARK: - Variables
    var arrCurrentPromoters = Array<Dictionary<String, AnyObject>>()
    var arrCurrentSelectedIndexPromoters:Array<Int> = []
    var event:Event!
    var arrDicPromoters:Array<Dictionary<String, AnyObject>> = []
    var arrDicUsers:Array<Dictionary<String, AnyObject>> = []
    //MARK: - IBOutlet
    @IBOutlet weak var assignBtn:UIButton!
    @IBOutlet weak var skipBtn:UIButton!
    @IBOutlet weak var numberPromotersLbl:UILabel!
    @IBOutlet weak var promotersTableView:UITableView!
    @IBOutlet weak var searchTxtFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let btnWidth = (self.view.frame.size.width - 30) / 2.0
        let btnHeight = btnWidth * 8 / 33
        let btnSize = CGSize(width: btnWidth, height: btnHeight)
        self.assignBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(btnSize), at: 0)
        self.skipBtn.layer.insertSublayer(ToolBox.createRedGradientLayerWithSize(btnSize), at: 0)
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(AdminAssignPromoterViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AdminAssignPromoterViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.arrCurrentPromoters = self.arrDicPromoters
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let viewCont = segue.destination as? AdminInviteFriendsViewController {
            viewCont.event = self.event
            viewCont.arrDicUsers = self.arrDicUsers
        }
    }
    //MARK: - IBAction
    @IBAction func backToMain(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func addContact(_ btn:UIButton){
        for currentIndex in self.arrCurrentSelectedIndexPromoters {
            if currentIndex == btn.tag {
                self.arrCurrentSelectedIndexPromoters.remove(at: self.arrCurrentSelectedIndexPromoters.index(of: currentIndex)!)
                self.promotersTableView.reloadData()
                self.updateNumberPromotersLbl()
                return
            }
        }
        self.arrCurrentSelectedIndexPromoters.append(btn.tag)
        self.promotersTableView.reloadData()
        self.updateNumberPromotersLbl()
        self.searchTxtFld.resignFirstResponder()
    }
    @IBAction func dismissKeyboard(){
        self.searchTxtFld.text = ""
        self.searchTxtFld.resignFirstResponder()
        self.arrCurrentPromoters = self.arrDicPromoters
        self.promotersTableView.reloadData()
    }
    @IBAction func assignPromoters(){
        if self.arrCurrentSelectedIndexPromoters.count > 0 {
            let users = self.arrCurrentSelectedIndexPromoters.map(){"\($0)"}.joined(separator: ",")
            let dicParams = ["action": "setPromotersToEvent", "event_id": "\(self.event.id.intValue)", "users": users]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                self.arrDicUsers = ToolBox.orderDicObjectsByName(arrDicObjects: self.arrDicUsers)
                                SVProgressHUD.dismiss()
                                self.performSegue(withIdentifier: "showInviteFriends", sender: nil)
                            }else{
                                SVProgressHUD.dismiss()
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            }
                            return
                        }
                    }
                }
                SVProgressHUD.dismiss()
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }else{
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Debe seleccionar uno o más promotores", inViewCont: self)
        }
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrCurrentPromoters.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "promoterCell", for: indexPath) as! ContactTableViewCell
        let dicPromoter = self.arrCurrentPromoters[(indexPath as NSIndexPath).row]
        let id = (dicPromoter["id"] as! NSString).integerValue
        let imgName = self.arrCurrentSelectedIndexPromoters.index(of: id) == nil ? "add" : "added"
        cell.addButton.setImage(UIImage(named: imgName), for: UIControlState())
        cell.contactImgView.strUrl = dicPromoter["photo_url"] as! String
        cell.contactNameLbl.text = dicPromoter["name"] as? String
        cell.addButton.tag = id
        return cell
    }
    //MARK: - Auxiliar
    func updateNumberPromotersLbl(){
        let numberFormattet = NumberFormatter()
        numberFormattet.minimumIntegerDigits = 2
        let number = NSNumber(value: self.arrCurrentSelectedIndexPromoters.count as Int)
        self.numberPromotersLbl.text = "\(numberFormattet.string(from: number)!) promotores añadidos"
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let totalHeight = self.view.frame.height
        let maxY = self.promotersTableView.frame.maxY
        let bottomSpace = totalHeight - maxY
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight - bottomSpace, right: 0)
        self.promotersTableView.contentInset = contentInset
        self.promotersTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.promotersTableView.contentInset = contentInset
        self.promotersTableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        if textField.text!.characters.count == 0 {
            self.arrCurrentPromoters = self.arrDicPromoters
            self.promotersTableView.reloadData()
        }else{
            let usersId = self.arrDicPromoters.map(){$0["id"] as! String}.joined(separator: ",")
            let dicParams = ["action": "searchUsers", "text_search": textField.text!, "users": usersId]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams).replacingOccurrences(of: " ", with: "+").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if let arrUsers = dicResult["users"] as? Array<Dictionary<String, AnyObject>> {
                                    self.arrDicPromoters.append(contentsOf: arrUsers)
                                    self.arrDicPromoters = ToolBox.orderDicObjectsByName(arrDicObjects: self.arrDicPromoters)
                                    self.arrCurrentPromoters = self.arrDicPromoters.filter(){($0["name"] as! String).range(of: textField.text!, options: .caseInsensitive, range: nil, locale: nil) != nil}
                                    self.promotersTableView.reloadData()
                                    SVProgressHUD.dismiss()
                                    return
                                }
                            }else{
                                SVProgressHUD.dismiss()
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
                SVProgressHUD.dismiss()
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }
        return true
    }
    func scrollViewDidScroll(_ scrollView:UIScrollView){
        self.searchTxtFld.resignFirstResponder()
    }
}
