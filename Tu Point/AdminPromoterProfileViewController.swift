//
//  AdminPromoterProfileViewController.swift
//  Tu Point
//
//  Created by victor salazar on 22/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminPromoterProfileViewController:CustomViewController{
    //MARK: - Variables
    var dicPromoter:Dictionary<String, AnyObject>!
    //MARK: - IBOutlet
    @IBOutlet weak var promoterImgView:FBImageView!
    @IBOutlet weak var promoterNameLbl:UILabel!
    @IBOutlet weak var promoterIdLbl:UILabel!
    @IBOutlet weak var promoterPhoneLbl:UILabel!
    @IBOutlet weak var promoterEmailLbl:UILabel!
    @IBOutlet weak var promoterAgeLbl:UILabel!
    @IBOutlet weak var promoterCivilStatusLbl:UILabel!
    @IBOutlet weak var promoterGenderLbl:UILabel!
    @IBOutlet weak var promoterCityLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicUser = self.dicPromoter["user"] as! Dictionary<String, AnyObject>
        self.promoterImgView.strUrl = dicUser["photo_url"] as! String
        self.promoterNameLbl.text = dicUser["name"] as? String
        self.promoterIdLbl.text = dicUser["id"] as? String
        self.promoterPhoneLbl.text = dicUser["phone"] as? String
        self.promoterEmailLbl.text = dicUser["email"] as? String
        self.promoterCivilStatusLbl.text = (ToolBox.getCivilStatuses().filter(){$0["code"] == (dicUser["civil_status"] as! String)}.first!)["title"]
        self.promoterGenderLbl.text = (ToolBox.getGenders().filter(){$0["code"] == (dicUser["sex"] as! String)}.first!)["title"]
        if let cityId = dicUser["city_id"] as? NSString {
            if let city = City.getCityById(cityId.integerValue) {
                self.promoterCityLbl.text = city.name
                return
            }
        }
    }
}
