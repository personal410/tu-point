//
//  ViewController.swift
//  Tu Point
//
//  Created by victor salazar on 15/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import FBSDKLoginKit
import Firebase
class LoginViewController:UIViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var logoImgView:UIImageView!
    @IBOutlet weak var loginFbBtn:UIButton!
    @IBOutlet weak var administratorBtn:UIView!
    //MARK: - ViewCont
    override func viewDidAppear(_ animated:Bool){
        super.viewDidAppear(animated)
        self.perform(#selector(LoginViewController.moveIcon), with: nil, afterDelay: 0.1)
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    //MARK: - IBAction
    @IBAction func loginFB(){
        let loginMan = FBSDKLoginManager()
        loginMan.logIn(withReadPermissions: ["public_profile", "email", "user_friends"]){(result:FBSDKLoginManagerLoginResult?, error:Error?) in
            if error == nil {
                if(result?.isCancelled)!{
                    ToolBox.showAlertWithTitle("Alerta", withMessage: "Se cancelo el inicio de sesión con Facebook", inViewCont: self)
                }else{
                    SVProgressHUD.show(withStatus: "Cargando")
                    FBSDKGraphRequest.init(graphPath: "/\(result!.token.userID!)", parameters: ["fields": "id, name, email, gender, birthday, friends"], httpMethod: "GET").start(completionHandler: { (con:FBSDKGraphRequestConnection?, result:Any?, error:Error?) in
                        if error == nil {
                            if let dicResult = result as? [String: AnyObject] {
                                var email:String = ""
                                if let emailTemp = dicResult["email"] as? String {
                                    email = emailTemp
                                }
                                var fbId:String = ""
                                if let fbIdTemp = dicResult["id"] as? String {
                                    fbId = fbIdTemp
                                }
                                var birthday = ""
                                if let birthdayTemp = dicResult["birthday"] as? String {
                                    birthday = birthdayTemp
                                }
                                var name:String = ""
                                if let nameTemp = dicResult["name"] as? String {
                                    name = nameTemp
                                }
                                var gender:String = ""
                                if let genderTemp = dicResult["gender"] as? String {
                                    gender = genderTemp
                                }
                                var friends:String = ""
                                if let dicFriends = dicResult["friends"] as? Dictionary<String, AnyObject> {
                                    if let arrData = dicFriends["data"] as? Array<Dictionary<String, String>> {
                                        let finalArrFriendsId = arrData.map(){$0["id"]!}
                                        friends = finalArrFriendsId.joined(separator: ",")
                                    }
                                }
                                UIApplication.shared.registerForRemoteNotifications()
                                
                                var fcm_token = ""
                                if let fcmToken = FIRInstanceID.instanceID().token() {
                                    fcm_token = fcmToken
                                }
                                let dicParams = ["action": "loginFacebook", "fcm_token": fcm_token, "email": email, "fb_id": fbId, "friends": friends, "name": name, "photo": "graph.facebook.com/\(fbId)/picture?type=large", "sex": gender, "birthday": birthday, "device": "iOS"]
                                let strParams = ToolBox.convertDictionaryToGetParams(dicParams).replacingOccurrences(of: " ", with: "+").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)!
                                ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                                    SVProgressHUD.dismiss()
                                    if error == nil {
                                        if let dicResult = result as? Dictionary<String, AnyObject> {
                                            if let state = dicResult["state"] as? String {
                                                if state == "success" {
                                                    if let dicUser = dicResult["user"] as? Dictionary<String, AnyObject> {
                                                        if User.createUser(dicUser, isFriend: false) {
                                                            if let arrCities = dicResult["cities"] as? Array<Dictionary<String, AnyObject>> {
                                                                if City.createCities(arrCities) {
                                                                    self.getFacebookImage()
                                                                    return
                                                                }
                                                            }
                                                        }
                                                    }
                                                }else{
                                                    let message = dicResult["message"] as! String
                                                    ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                                }
                                            }
                                        }
                                    }
                                    ToolBox.showErrorConnectionInViewCont(self)
                                })
                            }else{
                                SVProgressHUD.dismiss()
                                ToolBox.showErrorConnectionInViewCont(self)
                            }
                        }else{
                            SVProgressHUD.dismiss()
                            ToolBox.showErrorConnectionInViewCont(self)
                        }
                    })
                }
            }else{
                ToolBox.showErrorConnectionInViewCont(self)
            }
        }
    }
    //MARK: - Auxiliar
    func moveIcon(){
        let scaleTransform = CGAffineTransform(scaleX: 0.8, y: 0.8)
        UIView.animate(withDuration: 0.75, animations: {
            self.logoImgView.transform = scaleTransform
            self.loginFbBtn.alpha = 1.0
            self.administratorBtn.alpha = 1.0
        })
    }
    func getFacebookImage(){
        if let photoUrl = User.getUser()!.photo_url {
            if let data = try? Data(contentsOf: URL(string: photoUrl)!) {
                let fbId = User.getUser()!.fb_id!.intValue
                let filename = "\(fbId).png"
                let imageDirPath = ToolBox.getImagesDirectory()
                let imagePath = "\(imageDirPath)/\(filename)"
                try? data.write(to: URL(fileURLWithPath: imagePath), options: [])
                self.performSegue(withIdentifier: "showMainPage", sender: nil)
            }else{
                ToolBox.showErrorConnectionInViewCont(self)
            }
        }else{
            self.performSegue(withIdentifier: "showMainPage", sender: nil)
        }
    }
}
