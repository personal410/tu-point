//
//  AdminInviteFriendsViewController.swift
//  Tu Point
//
//  Created by victor salazar on 19/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminInviteFriendsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate {
    //MARK: - Variables
    var event:Event!
    var arrCurrentContacts = Array<Dictionary<String, AnyObject>>()
    var arrCurrentSelectedIndexContacts:Array<Int> = []
    var arrCurrentSelectedIndexVIPContacts:Array<Int> = []
    var arrDicUsers:Array<Dictionary<String, AnyObject>>?
    //MARK: - IBOutlet
    @IBOutlet weak var inviteBtn:UIButton!
    @IBOutlet weak var skipBtn:UIButton!
    @IBOutlet weak var contactsTableView:UITableView!
    @IBOutlet weak var searchTxtFld:UITextField!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let btnWidth = (self.view.frame.size.width - 30) / 2.0
        let btnHeight = btnWidth * 8 / 33
        let btnSize = CGSize(width: btnWidth, height: btnHeight)
        self.inviteBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(btnSize), at: 0)
        self.skipBtn.layer.insertSublayer(ToolBox.createRedGradientLayerWithSize(btnSize), at: 0)
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(AdminInviteFriendsViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AdminInviteFriendsViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        if let arrDicUsersTemp = arrDicUsers {
            self.arrCurrentContacts = arrDicUsersTemp
        }else{
            let dicParams = ["action": "getRandomUsers"]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if let arrDicUsersTemp = dicResult["users"] as? Array<Dictionary<String, AnyObject>> {
                                    self.arrDicUsers = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicUsersTemp)
                                }
                                self.arrCurrentContacts = self.arrDicUsers!
                                SVProgressHUD.dismiss()
                                self.contactsTableView.reloadData()
                                return
                            }else{
                                SVProgressHUD.dismiss()
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            }
                        }
                    }
                }
                SVProgressHUD.dismiss()
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }
    }
    //MARK: - IBAction
    @IBAction func backToMain(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    @IBAction func addContact(_ btn:UIButton){
        for currentIndex in self.arrCurrentSelectedIndexContacts {
            if currentIndex == btn.tag {
                self.arrCurrentSelectedIndexContacts.remove(at: self.arrCurrentSelectedIndexContacts.index(of: currentIndex)!)
                self.contactsTableView.reloadData()
                return
            }
        }
        self.arrCurrentSelectedIndexContacts.append(btn.tag)
        self.contactsTableView.reloadData()
        self.searchTxtFld.resignFirstResponder()
    }
    @IBAction func addVIPContact(_ btn:UIButton){
        for currentIndex in self.arrCurrentSelectedIndexVIPContacts {
            if currentIndex == btn.tag {
                self.arrCurrentSelectedIndexVIPContacts.remove(at: self.arrCurrentSelectedIndexVIPContacts.index(of: currentIndex)!)
                self.contactsTableView.reloadData()
                return
            }
        }
        self.arrCurrentSelectedIndexVIPContacts.append(btn.tag)
        self.contactsTableView.reloadData()
        self.searchTxtFld.resignFirstResponder()
    }
    @IBAction func dismissKeyboard(){
        self.searchTxtFld.text = ""
        self.searchTxtFld.resignFirstResponder()
        self.arrCurrentContacts = self.arrDicUsers!
        self.contactsTableView.reloadData()
    }
    @IBAction func invite(){
        if self.arrCurrentSelectedIndexContacts.count > 0 {
            let users = self.arrCurrentSelectedIndexContacts.map(){"\($0)"}.joined(separator: ",")
            let vips = self.arrCurrentSelectedIndexVIPContacts.map(){"\($0)"}.joined(separator: ",")
            let dicParams = ["action": "setInviteAttendees", "event_id": "\(self.event.id.intValue)", "users": users, "vips": vips]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                SVProgressHUD.dismiss()
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if let dicEventTemp = dicResult["event"] as? Dictionary<String, AnyObject> {
                                    if Event.editEvent(dicEventTemp) {
                                        _ = self.navigationController?.popToRootViewController(animated: true)
                                    }
                                    return
                                }
                                self.performSegue(withIdentifier: "showInviteFriends", sender: nil)
                                return
                            }else{
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }else{
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Debe seleccionar uno o más asistentes", inViewCont: self)
        }
    }
    @IBAction func skip(){
        _ = self.navigationController?.popToRootViewController(animated: true)
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrCurrentContacts.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "promoterCell", for: indexPath) as! ContactTableViewCell
        let dicContact = self.arrCurrentContacts[(indexPath as NSIndexPath).row]
        let id = (dicContact["id"] as! NSString).integerValue
        let imgName = self.arrCurrentSelectedIndexContacts.index(of: id) == nil ? "add" : "added"
        cell.addButton.setImage(UIImage(named: imgName), for: UIControlState())
        var vipImg = UIImage(named: "vip")!
        if self.arrCurrentSelectedIndexVIPContacts.index(of: id) == nil {
            vipImg = vipImg.withRenderingMode(.alwaysTemplate)
        }
        cell.VIPButton.setImage(vipImg, for: UIControlState())
        cell.contactImgView.strUrl = dicContact["photo_url"] as! String
        cell.contactNameLbl.text = dicContact["name"] as? String
        cell.addButton.tag = id
        cell.VIPButton.tag = id
        return cell
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let totalHeight = self.view.frame.height
        let maxY = self.contactsTableView.frame.maxY
        let bottomSpace = totalHeight - maxY
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight - bottomSpace, right: 0)
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        if textField.text!.characters.count == 0 {
            self.arrCurrentContacts = self.arrDicUsers!
            self.contactsTableView.reloadData()
        }else{
            let usersId = self.arrDicUsers!.map(){$0["id"] as! String}.joined(separator: ",")
            let dicParams = ["action": "searchUsers", "text_search": textField.text!, "users": usersId]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if let arrUsers = dicResult["users"] as? Array<Dictionary<String, AnyObject>> {
                                    self.arrDicUsers!.append(contentsOf: arrUsers)
                                    self.arrDicUsers = ToolBox.orderDicObjectsByName(arrDicObjects: self.arrDicUsers!)
                                    self.arrCurrentContacts = self.arrDicUsers! .filter(){($0["name"] as! String).range(of: textField.text!, options: .caseInsensitive, range: nil, locale: nil) != nil}
                                    SVProgressHUD.dismiss()
                                    self.contactsTableView.reloadData()
                                    return
                                }
                            }else{
                                SVProgressHUD.dismiss()
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
                SVProgressHUD.dismiss()
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }
        return true
    }
    func scrollViewDidScroll(_ scrollView:UIScrollView){
        self.searchTxtFld.resignFirstResponder()
    }
}
