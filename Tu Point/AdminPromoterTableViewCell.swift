//
//  AdminPromoterTableViewCell.swift
//  Tu Point
//
//  Created by Victor Salazar on 22/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminPromoterTableViewCell:UITableViewCell{
    @IBOutlet weak var promoterImgView:FBImageView!
    @IBOutlet weak var promoterNameLbl:UILabel!
    @IBOutlet weak var numberAssistantsLbl:UILabel!
    @IBOutlet weak var totalNumberAssistantsLbl:UILabel!
    @IBOutlet weak var numberListsLbl:UILabel!
    @IBOutlet weak var rankingLbl:UILabel!
    @IBOutlet weak var seeProfileCtrl:UIControl!
}