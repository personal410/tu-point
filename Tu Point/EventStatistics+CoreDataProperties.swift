//
//  EventStatistics+CoreDataProperties.swift
//  Tu Point
//
//  Created by Victor Salazar on 28/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension EventStatistics{
    @NSManaged var assistants:NSNumber
    @NSManaged var attend:NSNumber
    @NSManaged var committed:String
    @NSManaged var id:NSNumber
    @NSManaged var in_relationship:String
    @NSManaged var married:String
    @NSManaged var men:String
    @NSManaged var open_relation:String
    @NSManaged var single:String
    @NSManaged var undefined:String
    @NSManaged var undefined_relation:String
    @NSManaged var women:String
    @NSManaged var event:Event
}