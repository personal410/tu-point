//
//  ChoosePromoterViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 11/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ChoosePromoterViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var event:Event!
    var arrDicPromoters:Array<Dictionary<String, AnyObject>> =  []
    var arrDicFriends:Array<Dictionary<String, AnyObject>> =  []
    var arrDicUsers:Array<Dictionary<String, AnyObject>> =  []
    //MARK: - IBOutet
    @IBOutlet weak var promotersTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.loadPromoters()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? CreateListViewController {
            let row = sender as! Int
            viewCont.promoterId = (self.arrDicPromoters[row]["id"] as! NSString).integerValue
            viewCont.arrDicUsers = self.arrDicUsers
            viewCont.arrDicFriends = self.arrDicFriends
            viewCont.eventId = event.id.intValue
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        if self.arrDicPromoters.count == 0 {
            return 1
        }else{
            return self.arrDicPromoters.count
        }
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        if self.arrDicPromoters.count == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "noDataCell", for: indexPath)
            cell.isUserInteractionEnabled = false
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "promoterCell", for: indexPath) as! PromoterTableViewCell
            let dicPromoter = self.arrDicPromoters[(indexPath as NSIndexPath).row]
            let dicUser = dicPromoter["user"] as! Dictionary<String, AnyObject>
            let promoterId = (dicUser["id"] as! NSString).integerValue
            cell.promoterNameLbl.text = dicUser["name"] as? String
            let arrFriendIds = User.getFriends().map(){$0.id.intValue}
            let isFriend = (promoterId == User.getUser()!.id.intValue) ? true : ((event.public_promoters.boolValue) ? true : (arrFriendIds.index(of: promoterId) != nil))
            cell.promoterImgView.image = nil
            if isFriend {
                cell.promoterImgView.strUrl = dicUser["photo_url"] as! String
            }
            cell.isUserInteractionEnabled = isFriend
            cell.promoterImgView.backgroundColor = isFriend ? UIColor.clear : UIColor.lightGray
            let textColor = isFriend ? UIColor.white : UIColor(rgb: 81)
            cell.promoterNameLbl.textColor = textColor
            return cell
        }
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        self.performSegue(withIdentifier: "showCreateList", sender: (indexPath as NSIndexPath).row)
    }
    //MARK: - Auxiliar
    func loadPromoters(){
        let dicParams = ["action": "getEventPromotersAndFriends", "event_id": "\(event.id.intValue)", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicPromotersTemp = dicResult["promoters"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicPromoters = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicPromotersTemp)
                                self.promotersTableView.reloadData()
                            }
                            if let arrDicFriendsTemp = dicResult["friends"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicFriends = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicFriendsTemp)
                            }
                            if let arrDicUsersTemp = dicResult["users"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicUsers = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicUsersTemp)
                            }
                            SVProgressHUD.dismiss()
                            return
                        }else{
                            SVProgressHUD.dismiss()
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            SVProgressHUD.dismiss()
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
}
