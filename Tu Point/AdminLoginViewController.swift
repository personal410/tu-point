//
//  AdminLoginViewController.swift
//  Tu Point
//
//  Created by victor salazar on 16/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
import Firebase
class AdminLoginViewController:UIViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var userTxtFld:UITextField!
    @IBOutlet weak var passTxtFld:UITextField!
    @IBOutlet weak var loginBtn:CustomButton!
    @IBOutlet weak var returnBtn:CustomButton!
    @IBOutlet weak var loginView:UIView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.userTxtFld.attributedPlaceholder = NSAttributedString(string: "Usuario", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        self.passTxtFld.attributedPlaceholder = NSAttributedString(string: "Contraseña", attributes: [NSForegroundColorAttributeName: UIColor.lightGray])
        let btnSize = CGSize(width: self.view.frame.width - 120, height: 45)
        self.loginBtn.layer.insertSublayer(ToolBox.createBlueGradientLayerWithSize(btnSize), at: 0)
        self.returnBtn.layer.insertSublayer(ToolBox.createRedGradientLayerWithSize(btnSize), at: 0)
        NotificationCenter.default.addObserver(self, selector: #selector(AdminLoginViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(AdminLoginViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    //MARK: - IBAction
    @IBAction func dismissKeyBoard(){
        if !self.userTxtFld.resignFirstResponder() {
            self.passTxtFld.resignFirstResponder()
        }
    }
    @IBAction func returnToUserLogin(){
        if self.presentingViewController == nil {
            let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
            self.present(mainStoryboard.instantiateInitialViewController()!, animated: true, completion: nil)
        }else{
            if self.presentingViewController is LoginViewController {
                self.dismiss(animated: true, completion: nil)
            }else{
                let mainStoryboard = UIStoryboard(name: "Main", bundle: nil)
                self.present(mainStoryboard.instantiateInitialViewController()!, animated: true, completion: nil)
            }
        }
    }
    @IBAction func login(){
        let user = self.userTxtFld.text!
        let password = self.passTxtFld.text!
        if user.characters.count == 0 || password.characters.count == 0 {
            ToolBox.showAlertWithTitle("Alerta", withMessage: "Todos los campos son requeridos.", inViewCont: self)
        }else{
            let fcm_token = FIRInstanceID.instanceID().token()!
            let dicParams = ["action": "login", "email": user, "password": password, "fcm_token": fcm_token]
            let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
            SVProgressHUD.show(withStatus: "Cargando")
            ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", params: nil, response:{(result:AnyObject?, error:NSError?) -> Void in
                SVProgressHUD.dismiss()
                if error == nil {
                    if let dicResult = result as? Dictionary<String, AnyObject> {
                        if let state = dicResult["state"] as? String {
                            if state == "success" {
                                if let dicUser = dicResult["user"] as? Dictionary<String, AnyObject> {
                                    if User.createUser(dicUser, isFriend: false) {
                                        self.performSegue(withIdentifier: "showMainView", sender: nil)
                                        return
                                    }
                                }
                            }else{
                                let message = dicResult["message"] as! String
                                ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                                return
                            }
                        }
                    }
                }
                ToolBox.showErrorConnectionInViewCont(self)
            })
        }
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let bottomSpace = (self.view.frame.height - self.loginView.frame.height) / 2.0 + 110.0
        if bottomSpace < keyboardHeight {
            UIView.animate(withDuration: (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
                self.loginView.transform = CGAffineTransform(translationX: 0, y: bottomSpace - keyboardHeight);
            })
        }
    }
    func hideKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        UIView.animate(withDuration: (info[UIKeyboardAnimationDurationUserInfoKey] as! NSNumber).doubleValue, animations: {
            self.loginView.transform = CGAffineTransform.identity;
        })
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
