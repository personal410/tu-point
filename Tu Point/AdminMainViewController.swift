//
//  AdminMainViewController.swift
//  Tu Point
//
//  Created by victor salazar on 18/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminMainViewController:CustomViewController{
    //MARK: - IBOutlet
    @IBOutlet weak var welcomeLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        welcomeLbl.text = "bienvenido \(User.getUser()!.name)".uppercased()
    }
    //MARK: - IBAction
    @IBAction func backToAdminLogin(){
        let alertCont = UIAlertController(title: "Alerta", message: "¿Desea cerrar sesión?", preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "Cancelar", style: .default, handler: nil))
        alertCont.addAction(UIAlertAction(title: "Cerrar Sesión", style: .default, handler: { (action:UIAlertAction) -> Void in
            if User.deleteUser() {
                if self.presentingViewController == nil {
                    self.present(self.storyboard!.instantiateInitialViewController()!, animated: true, completion: nil)
                }else{
                    self.dismiss(animated: true, completion: nil)
                }
            }else{
                ToolBox.showAlertWithTitle("Alerta", withMessage: "Hubo un error al querer cerrar sesión", inViewCont: self)
            }
        }))
        self.present(alertCont, animated: true, completion: nil)
    }
    @IBAction func showEvents(){
        self.performSegue(withIdentifier: "showEvents", sender: nil)
    }
    @IBAction func showCreateEvent(){
        self.performSegue(withIdentifier: "showCreateEvent", sender: nil)
    }
    @IBAction func showPromoters(){
        self.performSegue(withIdentifier: "showListPromoters", sender: nil)
    }
}
