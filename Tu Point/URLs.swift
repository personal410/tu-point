//
//  URLs.swift
//  Sepadar
//
//  Created by Victor Salazar on 9/12/15.
//  Copyright © 2015 Victor Salazar. All rights reserved.
//
import Foundation
import CoreLocation
class URLs{
    static let key = "8ac7655079869fdd888e72fba66e6d0610572f8e"
    static let rootURL = "http://tupoint.peruappsdev.com/"
    static let rootGetURL = "\(rootURL)ws/\(key)?"
    static let rootPostURL = "\(rootURL)ws-post"
}