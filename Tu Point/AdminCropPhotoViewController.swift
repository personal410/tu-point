//
//  AdminCropPhotoViewController.swift
//  Tu Point
//
//  Created by victor salazar on 26/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminCropPhotoViewController:CustomViewController, UIScrollViewDelegate{
    //MARK: - Variables
    var originalImage:UIImage!
    var imgView:UIImageView!
    //MARK: - IBOutlet
    @IBOutlet weak var imgViewScrollView:UIScrollView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.imgViewScrollView.layer.borderColor = UIColor.white.cgColor
        self.imgViewScrollView.layer.borderWidth = 1
        imgView = UIImageView(image: originalImage)
        self.imgViewScrollView.contentSize = imgView.frame.size
        self.imgViewScrollView.addSubview(imgView)
        let imgViewScrollViewWidth = self.view.frame.width
        let imgViewScrollViewHeight = self.view.frame.width * 279.0 / 750.0
        let scaleWidth = imgViewScrollViewWidth / imgView.frame.width
        let scaleHeight = imgViewScrollViewHeight / imgView.frame.height
        let maxScale = max(scaleWidth, scaleHeight)
        imgViewScrollView.minimumZoomScale = maxScale
        imgViewScrollView.maximumZoomScale = 0.4267
        imgViewScrollView.zoomScale = maxScale
        if imgView.frame.width > imgViewScrollViewWidth {
            let x = (imgView.frame.width - imgViewScrollViewWidth) / 2.0
            imgViewScrollView.contentOffset = CGPoint(x: x, y: 0)
        }
        if imgView.frame.height > imgViewScrollViewHeight {
            let y = (imgView.frame.height - imgViewScrollViewHeight) / 2.0
            imgViewScrollView.contentOffset = CGPoint(x: 0, y: y)
        }
        centerScrollViewContents()
    }
    //MARK: - IBAction
    @IBAction func cancel(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    @IBAction func choose(){
        let scale1 = CGFloat(750.0 / 320.0)
        let size = CGSize(width: imgView.frame.width * scale1, height: imgView.frame.height * scale1)
        UIGraphicsBeginImageContext(size)
        originalImage.draw(in: CGRect(origin: CGPoint.zero, size: size))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        let clippedRect = CGRect(x: imgViewScrollView.contentOffset.x * scale1, y: imgViewScrollView.contentOffset.y * scale1, width: 750, height: 279)
        let imageRef = newImage?.cgImage?.cropping(to: clippedRect)
        let finalImage = UIImage(cgImage: imageRef!)
        let currentIndex = self.navigationController!.viewControllers.index(of: self)!
        if let viewCont = self.navigationController!.viewControllers[currentIndex - 1] as? AdminCreateEventViewController {
            viewCont.finalImage = finalImage
        }
        _ = self.navigationController?.popViewController(animated: true)
    }
    //MARK: - ScrollView
    func viewForZooming(in scrollView:UIScrollView) -> UIView? {
        return imgView
    }
    func scrollViewDidZoom(_ scrollView:UIScrollView){
        centerScrollViewContents()
    }
    //MARK: - Auxiliar
    func centerScrollViewContents(){
        let imgViewScrollViewWidth = self.view.frame.width
        let imgViewScrollViewHeight = self.view.frame.width * 279.0 / 750.0
        var contentsFrame = imgView.frame
        if contentsFrame.width < imgViewScrollViewWidth {
            contentsFrame.origin.x = (imgViewScrollViewWidth - contentsFrame.width) / 2
        }else{
            contentsFrame.origin.x = 0
        }
        if contentsFrame.height < imgViewScrollViewHeight {
            contentsFrame.origin.y = (imgViewScrollViewHeight - contentsFrame.height) / 2
        }else{
            contentsFrame.origin.y = 0
        }
        imgView.frame = contentsFrame
    }
}
