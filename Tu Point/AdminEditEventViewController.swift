//
//  AdminEditEventViewController.swift
//  Tu Point
//
//  Created by victor salazar on 22/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminEditEventViewController:CustomViewController{
    //MARK: - Variables
    var event:Event!
    //MARK: - IBOutlet
    @IBOutlet weak var eventNameLbl:UILabel!
    @IBOutlet weak var createByLbl:UILabel!
    @IBOutlet weak var eventDateLbl:UILabel!
    @IBOutlet weak var addressLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        eventNameLbl.text = event.name
        createByLbl.text = "Creado por \(User.getUser()!.name)"
        let dateFormatter = ToolBox.getDateFormatter()
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: event.date_time_start).capitalized
        let dateComps = (Calendar.current as NSCalendar).components([.day, .year], from: event.date_time_start as Date)
        eventDateLbl.text = "\(dateComps.day) de \(month) del \(dateComps.year)"
        addressLbl.text = event.disco.address
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if segue.identifier == "showCreateEvent" {
            let viewCont = segue.destination as! AdminCreateEventViewController
            viewCont.event = event
        }else if let viewCont = segue.destination as? AdminInviteFriendsViewController {
            viewCont.event = self.event
        }
    }
    //MARK: - IBAction
    @IBAction func editEvent(){
        self.performSegue(withIdentifier: "showCreateEvent", sender: nil)
    }
    @IBAction func inviteGuests(){
        self.performSegue(withIdentifier: "showInviteGuests", sender: nil)
    }
}
