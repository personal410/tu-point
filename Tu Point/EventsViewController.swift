//
//  EventsViewController.swift
//  Tu Point
//
//  Created by victor salazar on 20/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class EventsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate{
    //MARK: - Variables
    var currentLocalIndex:Int!
    var currentCityIndex:Int!
    var currentList:Int = 0
    var shouldShowProgress = true
    var arrEvents:Array<Event> = Event.getEvents()!
    var arrCurrentEvents:Array<Event> = []
    var arrDicLocals:Array<Dictionary<String, AnyObject>> = []
    var arrSelectedLocals:Array<Int> = []
    var arrDicCities:Array<Dictionary<String, AnyObject>> = []
    var arrSelectedCities:Array<Int> = []
    let refreshCtrl = UIRefreshControl()
    //MARK: - IBOutlet
    @IBOutlet weak var eventsTableView:UITableView!
    @IBOutlet weak var listTableView:UITableView!
    @IBOutlet weak var gradientView:UIView!
    @IBOutlet weak var filterBtn:UIButton!
    @IBOutlet weak var titleGradientLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.filterBtn.layer.borderColor = UIColor.white.cgColor
        let colorTop = UIColor(rgb: 0, a: 0.2).cgColor
        let colorMiddle = UIColor(rgb: 0, a: 0.85).cgColor
        let colorBottom = UIColor(rgb: 0, a: 0.85).cgColor
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame.origin = CGPoint(x: 0, y: 0)
        gradientLayer.frame.size = CGSize(width: self.view.frame.width, height: self.view.frame.height - self.gradientView.frame.origin.y)
        gradientLayer.colors = [colorTop, colorMiddle, colorBottom]
        gradientLayer.locations = [0, 0.4, 1]
        self.gradientView.layer.insertSublayer(gradientLayer, at: 0)
        refreshCtrl.addTarget(self, action: #selector(EventsViewController.refreshEvents), for: .valueChanged)
        self.eventsTableView.addSubview(refreshCtrl)
        self.getEvents()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? EventDetailViewController {
            let row = (eventsTableView.indexPathForSelectedRow! as NSIndexPath).row
            viewCont.event = arrEvents[row]
        }
    }
    //MARK: - IBAction
    @IBAction func showList(_ btn:UIButton){
        if self.gradientView.isHidden {
            self.currentList = btn.tag
            self.listTableView.reloadData()
            self.titleGradientLbl.text = self.currentList == 0 ? "Eliges los locales que quieres ver" : "Eliges las ciudades que quieres ver:"
            self.gradientView.transform = CGAffineTransform(translationX: 0, y: self.gradientView.frame.height)
            self.gradientView.isHidden = false
            UIView.animate(withDuration: 0.25, animations: {
                self.gradientView.transform = CGAffineTransform.identity
            })
        }else{
            if currentList == btn.tag {
                UIView.animate(withDuration: 0.25, animations: {
                    self.gradientView.transform = CGAffineTransform(translationX: 0, y: self.gradientView.frame.height)
                }, completion:{(b:Bool) in
                    self.gradientView.transform = CGAffineTransform.identity
                    self.gradientView.isHidden = true
                })
            }else{
                self.titleGradientLbl.text = self.currentList == 0 ? "Eliges los locales que quieres ver" : "Eliges las ciudades que quieres ver:"
                self.currentList = btn.tag
                self.listTableView.reloadData()
            }
        }
    }
    @IBAction func dismissList(){
        self.gradientView.isHidden = true
        self.filter()
    }
    @IBAction func filter(){
        if self.arrSelectedLocals.count > 0 || self.arrSelectedCities.count > 0 {
            self.arrCurrentEvents = []
            
            var arrLocalIds:Array<Int> = []
            var arrCitiesIds:Array<Int> = []
            if self.arrSelectedLocals.count > 0 {
                var arrSelectedDicLocals = Array<Dictionary<String, AnyObject>>()
                for indexSelectedLocal in arrSelectedLocals {
                    arrSelectedDicLocals.append(arrDicLocals[indexSelectedLocal])
                }
                arrLocalIds = arrSelectedDicLocals.map(){($0["id"] as! NSString).integerValue}
            }
            if self.arrSelectedCities.count > 0 {
                var arrSelectedDicCities = Array<Dictionary<String, AnyObject>>()
                for indexSelectedCity in arrSelectedCities {
                    arrSelectedDicCities.append(arrDicCities[indexSelectedCity])
                }
                arrCitiesIds = arrSelectedDicCities.map(){($0["id"] as! NSString).integerValue}
            }
            for event in self.arrEvents {
                if arrLocalIds.index(of: event.disco.id.intValue) != nil || arrCitiesIds.index(of: event.city_id.intValue) != nil {
                    self.arrCurrentEvents.append(event)
                }
            }
            self.eventsTableView.reloadData()
        }else{
            self.arrCurrentEvents = self.arrEvents
            self.eventsTableView.reloadData()
        }
        self.gradientView.isHidden = true
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        if tableView == eventsTableView {
            return self.arrCurrentEvents.count
        }else{
            if currentList == 0 {
                return arrDicLocals.count
            }else{
                return arrDicCities.count
            }
        }
    }
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        if tableView == eventsTableView {
            return 140.0 * self.view.frame.width / 375.0
        }else{
            return 60.0
        }
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        if tableView == eventsTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventTableViewCell
            let event = self.arrCurrentEvents[(indexPath as NSIndexPath).row]
            cell.eventImgView.image = nil
            cell.eventImgView.strUrl = event.photo_url
            cell.discoNameLbl.text = event.disco.name
            let dateTimeStart = event.date_time_start
            let dateComps = (Calendar.current as NSCalendar).components([.weekday, .day, .month], from: dateTimeStart as Date)
            cell.dayWeekLbl.text = ToolBox.convertDateToString(dateTimeStart, withFormat: "EEEE").capitalized
            cell.dayLbl.text = "\(dateComps.day!)"
            cell.monthLbl.text = ToolBox.convertDateToString(dateTimeStart, withFormat: "MMMM").capitalized
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "itemCell", for: indexPath) as! ItemListTableViewCell
            let currentArray = self.currentList == 0 ? self.arrDicLocals : self.arrDicCities
            let currentSelectedArray = self.currentList == 0 ? self.arrSelectedLocals : self.arrSelectedCities
            cell.titleLbl.text = currentArray[(indexPath as NSIndexPath).row]["name"] as? String
            cell.checkImgView.isHidden = !currentSelectedArray.contains((indexPath as NSIndexPath).row)
            return cell
        }
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        if tableView != self.eventsTableView {
            if self.currentList == 0 {
                if self.arrSelectedLocals.contains((indexPath as NSIndexPath).row) {
                    self.arrSelectedLocals.remove(at: self.arrSelectedLocals.index(of: (indexPath as NSIndexPath).row)!)
                }else{
                    self.arrSelectedLocals.append((indexPath as NSIndexPath).row)
                }
                self.listTableView.reloadRows(at: [indexPath], with: .none)
            }else{
                if self.arrSelectedCities.contains((indexPath as NSIndexPath).row) {
                    self.arrSelectedCities.remove(at: self.arrSelectedCities.index(of: (indexPath as NSIndexPath).row)!)
                }else{
                    self.arrSelectedCities.append((indexPath as NSIndexPath).row)
                }
                self.listTableView.reloadRows(at: [indexPath], with: .none)
            }
        }
    }
    //MARK: - Auxiliar
    func refreshEvents(){
        shouldShowProgress = false
        self.getEvents()
    }
    func getEvents(){
        let dicParams = ["action": "getPublicEvents"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        if shouldShowProgress {
            SVProgressHUD.show(withStatus: "Cargando")
        }
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            self.shouldShowProgress = true
            self.refreshCtrl.endRefreshing()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicEventsTemp = dicResult["events"] as? Array<Dictionary<String, AnyObject>> {
                                let currentEvents = Event.getEvents()!
                                var currentEventIds = currentEvents.map(){$0.id.intValue}
                                for dicEvent in arrDicEventsTemp {
                                    let id = (dicEvent["id"] as! NSString).integerValue
                                    if let index = currentEventIds.index(of: id) {
                                        currentEventIds.remove(at: index)
                                        _ = Event.editEvent(dicEvent)
                                    }else{
                                        _ = Event.createEvent(dicEvent)
                                    }
                                }
                                for eventId in currentEventIds {
                                    if let event = Event.getEventById(eventId) {
                                        let arrEventToAssit = User.getUser()!.in_events!.components(separatedBy: ",").map(){($0 as NSString).integerValue}
                                        var eventDeleted = false
                                        if arrEventToAssit.index(of: eventId) == nil {
                                            let arrEventWithList = User.getUser()!.event_lists!.components(separatedBy: ",").map(){($0 as NSString).integerValue}
                                            if arrEventWithList.index(of: eventId) == nil {
                                                Event.deleteEventWithId(eventId)
                                                eventDeleted = true
                                            }
                                        }
                                        if !eventDeleted {
                                            Event.setDontShowToEvent(event)
                                        }
                                    }
                                }
                                self.arrEvents = Event.getEvents()!
                                self.arrCurrentEvents = self.arrEvents
                            }
                            if let arrDicCitiesTemp = dicResult["locals"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicCities = arrDicCitiesTemp
                            }
                            if let arrDicLocalsTemp = dicResult["discos"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrDicLocals = arrDicLocalsTemp
                            }
                            self.eventsTableView.reloadData()
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            return
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
}
