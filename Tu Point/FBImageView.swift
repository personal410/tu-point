//
//  FBImageView.swift
//  Tu Point
//
//  Created by Victor Salazar on 31/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class FBImageView:CustomImageView{
    let actityIndicator = UIActivityIndicatorView(frame: CGRect.zero)
    var strUrl:String {
        didSet{
            if strUrl != "" {
                let url = URL(string: strUrl)!
                let filename = url.pathComponents[url.pathComponents.count - 2] + ".png"
                let imageDirPath = ToolBox.getImagesDirectory()
                let imagePath = "\(imageDirPath)/\(filename)"
                if FileManager.default.fileExists(atPath: imagePath) {
                    self.image = UIImage(contentsOfFile: imagePath)
                    self.backgroundColor = UIColor.clear
                    self.actityIndicator.stopAnimating()
                    self.actityIndicator.isHidden = true
                }else{
                    self.backgroundColor = UIColor.lightGray
                    self.actityIndicator.isHidden = false
                    actityIndicator.startAnimating()
                    let imageUrl = strUrl
                    DispatchQueue.global(qos: .background).async {
                        let imageData = try! Data(contentsOf: URL(string: imageUrl)!)
                        if !FileManager.default.fileExists(atPath: imagePath) {
                            try? imageData.write(to: URL(fileURLWithPath: imagePath), options: [])
                        }
                        DispatchQueue.main.async(execute: {
                            let url2 = URL(string: self.strUrl)!
                            let filename2 = url2.pathComponents[url2.pathComponents.count - 2] + ".png"
                            if filename2 == filename {
                                self.actityIndicator.stopAnimating()
                                self.actityIndicator.isHidden = true
                                self.backgroundColor = UIColor.clear
                                self.image = UIImage(data: imageData)
                            }
                        })
                    }
                }
            }else{
                image = nil
            }
        }
    }
    required init?(coder aDecoder:NSCoder){
        strUrl = ""
        super.init(coder:aDecoder)
        actityIndicator.activityIndicatorViewStyle = self.frame.size.width < 40 && self.frame.size.height < 40 ? .white : .whiteLarge
        addSubview(actityIndicator)
    }
    override func layoutSubviews(){
        super.layoutSubviews()
        actityIndicator.frame = bounds
    }
}
