//
//  NotificationsViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 26/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class NotificationsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var arrNotifications = Array<Dictionary<String, AnyObject>>()
    //MARK: -  IBOutlet
    @IBOutlet weak var notificationsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicParams = ["action": "getNotifications", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let arrResult = result as? Array<Dictionary<String, AnyObject>>{
                    self.arrNotifications = arrResult
                    self.notificationsTableView.reloadData()
                    return
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? EventDetailViewController {
            viewCont.event = sender as! Event
        }else{
            segue.destination.setValue(sender, forKey: "dicList")
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrNotifications.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell", for: indexPath) as! NotificationTableViewCell
        let dicNotification = self.arrNotifications[(indexPath as NSIndexPath).row]
        cell.notificationStateImgView.image = UIImage(named: "unreadedNotification")
        cell.titleLbl.text = dicNotification["name"] as? String
        cell.descriptionLbl.text = dicNotification["description"] as? String
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAt indexPath:IndexPath){
        let dicNotification = self.arrNotifications[(indexPath as NSIndexPath).row]
        let model = dicNotification["model"] as! String
        if model == "event" {
            let id = (dicNotification["model_id"] as! NSString).integerValue
            var event = Event.getEventById(id)
            if event == nil {
                if let modelObject = dicNotification["model_object"] as? Dictionary<String, AnyObject> {
                    event = Event.createEvent(modelObject)
                }
            }
            if event != nil {
                self.performSegue(withIdentifier: "showEvent", sender: event)
            }
        }else if model == "list"{
            let dicList = dicNotification["model_object"] as! Dictionary<String, AnyObject>
            let state = dicList["state"] as! String
            if state == "wait" {
                self.performSegue(withIdentifier: "showCheckList", sender: dicList)
            }else if state == "accepted" {
                self.performSegue(withIdentifier: "showFinalListDetail", sender: dicList)
            }
        }
    }
}
