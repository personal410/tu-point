//
//  CustomViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 23/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class CustomViewController:UIViewController{
    @IBAction func showMenu(){
        if let mainViewCont = self.parent as? MainViewController {
            mainViewCont.showMenu(0.5)
        }else if let mainViewCont = self.navigationController?.parent as? MainViewController {
            mainViewCont.showMenu(0.5)
        }
    }
    @IBAction func back(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
}
