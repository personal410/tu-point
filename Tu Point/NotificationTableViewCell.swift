//
//  NotificationTableViewCell.swift
//  Tu Point
//
//  Created by victor salazar on 27/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class NotificationTableViewCell:UITableViewCell{
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var descriptionLbl:UILabel!
    @IBOutlet weak var notificationStateImgView:UIImageView!
}