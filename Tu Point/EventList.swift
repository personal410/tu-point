//
//  EventList.swift
//  Tu Point
//
//  Created by victor salazar on 1/2/17.
//  Copyright © 2017 victor salazar. All rights reserved.
//
import Foundation
class EventList{
    var eventListId:String = "0"
    var listState:String = ""
    var arrGuest:Array<Dictionary<String, AnyObject>> = []
    var arrAcceptedGuest:Array<Dictionary<String, AnyObject>> = []
    var user:Dictionary<String, AnyObject> = [:]
    init(dicList:Dictionary<String, AnyObject>){
        if let id = dicList["id"] as? String {
            eventListId = id
        }
        if let state = dicList["state"] as? String {
            listState = state
        }
        if let arrDicGuestsTemp = dicList["guests"] as? Array<Dictionary<String, AnyObject>> {
            arrGuest = arrDicGuestsTemp
            for dicGuestTemp in arrGuest {
                let accepted = dicGuestTemp["accepted"]
                var finalAccepted = false
                if let numberAccepted = accepted as? NSNumber {
                    finalAccepted = numberAccepted.boolValue
                }else if let strAccepted = accepted as? NSString {
                    finalAccepted = strAccepted.boolValue
                }
                if finalAccepted {
                    arrAcceptedGuest.append(dicGuestTemp)
                }
            }
        }
        if let userTemp = dicList["user"] as? Dictionary<String, AnyObject> {
            user = userTemp
        }
    }
    static func transformArrDicList(arrDicLists:Array<Dictionary<String, AnyObject>>) -> Array<EventList> {
        var arrEventLists:Array<EventList> = []
        for dicList in arrDicLists {
            arrEventLists.append(EventList(dicList: dicList))
        }
        return arrEventLists
    }
}
