//
//  AdminEventsViewController.swift
//  Tu Point
//
//  Created by victor salazar on 18/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class AdminEventsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var arrEvents = Event.getEvents()!
    //MARK: - IBOutlet
    @IBOutlet weak var eventsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicParams = ["action": "getAdminEvents", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicEventsTemp = dicResult["events"] as? Array<Dictionary<String, AnyObject>> {
                                let currentEvents = Event.getEvents()!
                                var currentEventIds = currentEvents.map(){$0.id.intValue}
                                for dicEvent in arrDicEventsTemp {
                                    let id = (dicEvent["id"] as! NSString).integerValue
                                    if let index = currentEventIds.index(of: id) {
                                        currentEventIds.remove(at: index)
                                        _ = Event.editEvent(dicEvent)
                                    }else{
                                        _ = Event.createEvent(dicEvent)
                                    }
                                }
                                for eventId in currentEventIds {
                                    Event.deleteEventWithId(eventId)
                                }
                                self.arrEvents = Event.getEvents()!.reversed()
                            }
                            self.eventsTableView.reloadData()
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                        return
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? EventDetailViewController {
            viewCont.event = self.arrEvents[(self.eventsTableView.indexPathForSelectedRow! as NSIndexPath).row]
        }else if let viewCont = segue.destination as? AdminEditEventViewController {
            let ctrl = sender as! UIControl
            viewCont.event = arrEvents[ctrl.tag]
        }
    }
    //MARK: - IBAction
    @IBAction func editEvent(_ ctrl:UIControl){
        self.performSegue(withIdentifier: "showEditEvent", sender: ctrl)
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForRowAt indexPath:IndexPath) -> CGFloat {
        return 145.0 * self.view.frame.width / 375.0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrEvents.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventCell", for: indexPath) as! EventTableViewCell
        let event = self.arrEvents[(indexPath as NSIndexPath).row]
        cell.eventImgView.image = nil
        cell.eventImgView.strUrl = event.photo_url
        cell.editEventCtrl.tag = (indexPath as NSIndexPath).row
        return cell
    }
}
