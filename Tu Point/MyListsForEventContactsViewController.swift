//
//  MyListsForEventContactsViewController.swift
//  Tu Point
//
//  Created by victor salazar on 12/11/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyListsForEventContactsViewController:CustomViewController, UITextFieldDelegate {
    //MARK: - Variables
    //var arrDicLists:Array<Dictionary<String, AnyObject>>!
    var arrLists:Array<EventList> = []
    var arrCurrentDicGuests:Array<Dictionary<String, AnyObject>> = []
    var arrDicGuests:Array<Dictionary<String, AnyObject>> = []
    //MARK: - IBOutlet
    @IBOutlet weak var alreadyHereLbl:UILabel!
    @IBOutlet weak var willAssistLbl:UILabel!
    @IBOutlet weak var searchTxtFld:UITextField!
    @IBOutlet weak var contactsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        for list in self.arrLists {
            self.arrDicGuests.append(contentsOf: list.arrAcceptedGuest)
        }
        self.arrDicGuests = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicGuests)
        self.arrCurrentDicGuests = self.arrDicGuests
        var count = 0
        for dicGuest in self.arrDicGuests {
            let state = dicGuest["state"]
            var finalState = false
            if let numberState = state as? NSNumber {
                finalState = numberState.boolValue
            }else if let strState = state as? NSString {
                finalState = strState.boolValue
            }
            if finalState {
                count += 1
            }
        }
        self.alreadyHereLbl.text = "\(count)"
        self.willAssistLbl.text = "\(arrDicGuests.count)"
        self.contactsTableView.reloadData()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        segue.destination.setValue(sender, forKey: "dicList")
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return arrCurrentDicGuests.count
    }
    func tableView(_ tableView:UITableView, cellForRowAtIndexPath indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        let dicGuest = self.arrCurrentDicGuests[(indexPath as NSIndexPath).row]
        cell.contactNameLbl.text = dicGuest["name"] as? String
        cell.contactImgView.strUrl = dicGuest["photo_url"] as! String
        let state = dicGuest["state"]
        var finalState = false
        if let numberState = state as? NSNumber {
            finalState = numberState.boolValue
        }else if let strState = state as? NSString {
            finalState = strState.boolValue
        }
        cell.addButton.isHidden = !finalState
        return cell
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.characters.count == 0 {
            self.arrCurrentDicGuests = self.arrDicGuests
        }else{
            self.arrCurrentDicGuests = self.arrDicGuests.filter(){($0["name"] as! String).lowercased().range(of: newString.lowercased(), options: .caseInsensitive, range: nil, locale: nil) != nil}
        }
        self.contactsTableView.reloadData()
        return true
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0)
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
}
