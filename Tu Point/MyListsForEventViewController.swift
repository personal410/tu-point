//
//  MyListsForEventViewController.swift
//  Tu Point
//
//  Created by victor salazar on 12/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyListsForEventViewController:CustomViewController {
    //MARK: - Variables
    var dicEvent:Dictionary<String, AnyObject>!
    var arrLists:Array<EventList> = []
    //MARK: - IBOutlet
    @IBOutlet weak var alreadyHereLbl:UILabel!
    @IBOutlet weak var willAssistLbl:UILabel!
    @IBOutlet weak var myListsTableView:UITableView!
    //MARK: - ViewCont
    override func viewWillAppear(_ animated:Bool){
        super.viewWillAppear(animated)
        SVProgressHUD.show(withStatus: "Cargando")
        let dicParams = ["action": "getMyLists", "user_id": "\(User.getUser()!.id.intValue)", "event_id": self.dicEvent["id"] as! String]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicEventsTemp = dicResult["event_lists"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrLists = EventList.transformArrDicList(arrDicLists: arrDicEventsTemp)
                                var count = 0
                                var totalCount = 0
                                for eventList in self.arrLists {
                                    totalCount += eventList.arrAcceptedGuest.count
                                    for dicGuest in eventList.arrAcceptedGuest {
                                        let state = dicGuest["state"]
                                        var finalState = false
                                        if let numberState = state as? NSNumber {
                                            finalState = numberState.boolValue
                                        }else if let strState = state as? NSString {
                                            finalState = strState.boolValue
                                        }
                                        if finalState {
                                            count += 1
                                        }
                                    }
                                }
                                self.alreadyHereLbl.text = "\(count)"
                                self.willAssistLbl.text = "\(totalCount)"
                                SVProgressHUD.dismiss()
                                self.myListsTableView.reloadData()
                            }
                            return
                        }else{
                            SVProgressHUD.dismiss()
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                            return
                        }
                    }
                }
            }
            SVProgressHUD.dismiss()
            ToolBox.showErrorConnectionInViewCont(self)
        })
        self.myListsTableView.reloadData()
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? MyListsForEventContactsViewController {
            viewCont.arrLists = arrLists
        }else if let viewCont = segue.destination as? FinalListViewController {
            viewCont.eventList = sender as! EventList
        }else if let viewCont = segue.destination as? CheckListViewController {
            viewCont.eventList = sender as! EventList
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrLists.count
    }
    func tableView(_ tableView:UITableView, cellForRowAtIndexPath indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "listCell", for: indexPath) as! MyListDetailTableViewCell
        let eventList = arrLists[indexPath.row]
        cell.senderLbl.text = eventList.user["name"] as? String
        cell.numberGuestLbl.text = "Número de asistentes: \(eventList.arrAcceptedGuest.count) persona\(eventList.arrAcceptedGuest.count == 1 ? "" : "s")"
        let state = eventList.listState
        if state == "wait" {
            cell.listStatusImgView.isHidden = true
            cell.rightSpaceCons.constant = 15
        }else{
            cell.rightSpaceCons.constant = 85
            cell.listStatusImgView.isHidden = false
            cell.listStatusImgView.image = UIImage(named: "list\(state.capitalized)")
        }
        return cell
    }
    func tableView(_ tableView:UITableView, didSelectRowAtIndexPath indexPath:IndexPath){
        let eventList = arrLists[indexPath.row]
        if eventList.listState == "wait" {
            self.performSegue(withIdentifier: "showCheckList", sender: eventList)
        }else if eventList.listState == "accepted" {
            self.performSegue(withIdentifier: "showFinalListDetail", sender: eventList)
        }
    }
}
