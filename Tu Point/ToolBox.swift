//
//  ToolBox.swift
//  Tu Point
//
//  Created by victor salazar on 19/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import UIKit
class ToolBox{
    class func createGradientLayerWithSize(_ size:CGSize, withColors colors:[CGColor], withCornerRadius cornerRadius:CGFloat = 5) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.cornerRadius = cornerRadius
        gradientLayer.frame.size = size
        gradientLayer.colors = colors
        return gradientLayer
    }
    class func createBlueGradientLayerWithSize(_ size:CGSize, withCornerRadius cornerRadius:CGFloat = 5) -> CAGradientLayer {
        let topColor = UIColor(r: 9, g: 25, b: 32).cgColor
        let bottomColor = UIColor(r: 0, g: 57, b: 102).cgColor
        return self.createGradientLayerWithSize(size, withColors: [topColor, bottomColor], withCornerRadius: cornerRadius)
    }
    class func createRedGradientLayerWithSize(_ size:CGSize, withCornerRadius cornerRadius:CGFloat = 5) -> CAGradientLayer {
        let topColor = UIColor(r: 9, g: 7, b: 7).cgColor
        let bottomColor = UIColor(r: 76, g: 0, b: 2).cgColor
        return self.createGradientLayerWithSize(size, withColors: [topColor, bottomColor], withCornerRadius: cornerRadius)
    }
    class func showAlertWithTitle(_ title:String, withMessage message:String, withOkHandler handler:((_ alertAction:UIAlertAction) -> Void)? = nil, inViewCont viewCont:UIViewController){
        let alertCont = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertCont.addAction(UIAlertAction(title: "OK", style: .default, handler: handler))
        viewCont.present(alertCont, animated: true, completion: nil)
    }
    class func showErrorConnectionInViewCont(_ viewCont:UIViewController){
        self.showAlertWithTitle("Alerta", withMessage: "Hubo error en la conexión.", inViewCont: viewCont)
    }
    class func convertDictionaryToGetParams(_ dictionary:Dictionary<String, String>) -> String {
        var arrParams = Array<String>()
        for (key, value) in dictionary {
            arrParams.append("\(key)=\(value)")
        }
        return arrParams.joined(separator: "&")
    }
    class func convertDateToString(_ date:Date, withFormat format:String = "yyyy-MM-dd HH:mm:00") -> String {
        let dateFormatter = self.getDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter.string(from: date)
    }
    class func convertStringToDate(_ string:String) -> Date {
        let dateFormatter = self.getDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.date(from: string)!
    }
    class func getDateFormatter() -> DateFormatter {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "es_ES")
        dateFormatter.calendar = Calendar.init(identifier: Calendar.Identifier.gregorian)
        return dateFormatter
    }
    class func getApplicationDocumentDirectory() -> String{
        return NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
    }
    class func getImagesDirectory() -> String {
        return "\(self.getApplicationDocumentDirectory())/images"
    }
    class func getCivilStatuses() -> Array<Dictionary<String, String>>{
        return [["code": "single", "title": "Soltero"], ["code": "married", "title": "Casado"], ["code": "in_relationship", "title": "En una relación"], ["code": "committed", "title": "Comprometido"], ["code": "open_relationship", "title": "Relación abierta"], ["code": "not_define", "title": "No definido"]]
    }
    class func getGenders() -> Array<Dictionary<String, String>>{
        return [["code": "male", "title": "Masculino"], ["code": "female", "title": "Femenino"], ["code": "undefined", "title": "No definido"]]
    }
    class func orderDicObjectsByName(arrDicObjects:Array<Dictionary<String, AnyObject>>) -> Array<Dictionary<String, AnyObject>> {
        var arrDicObjectsFinal:Array<Dictionary<String, AnyObject>> = []
        for dicObjectTemp in arrDicObjects {
            var inserted = false
            if let objectNameTemp = dicObjectTemp["name"] as? String {
                for (index, dicObject) in arrDicObjectsFinal.enumerated() {
                    let objectName = dicObject["name"] as! String
                    if objectNameTemp < objectName {
                        inserted = true
                        arrDicObjectsFinal.insert(dicObjectTemp, at: index)
                        break
                    }
                }
            }
            if !inserted {
                arrDicObjectsFinal.append(dicObjectTemp)
            }
        }
        return arrDicObjectsFinal
    }
}
