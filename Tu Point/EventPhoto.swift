//
//  EventPhoto.swift
//  Tu Point
//
//  Created by victor salazar on 23/04/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import Foundation
import CoreData
class EventPhoto:NSManagedObject{
    class func createEventPhoto(_ photoUrl:String, event:Event){
        let ctxt = AppDelegate.getMoc()
        let newEventPhoto = NSEntityDescription.insertNewObject(forEntityName: "EventPhoto", into: ctxt) as! EventPhoto
        newEventPhoto.photoUrl = photoUrl
        newEventPhoto.event = event
        do{
            try ctxt.save()
        }catch let error as NSError {
            print(error)
        }
    }
}
