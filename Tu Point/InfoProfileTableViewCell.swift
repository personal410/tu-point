//
//  InfoProfileTableViewCell.swift
//  Tu Point
//
//  Created by Victor Salazar on 24/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class InfoProfileTableViewCell:UITableViewCell{
    @IBOutlet weak var titleLbl:UILabel!
    @IBOutlet weak var valueTxtFld:UITextField!
    @IBOutlet weak var editCtrl:UIControl!
}