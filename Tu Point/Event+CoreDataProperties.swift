//
//  Event+CoreDataProperties.swift
//  Tu Point
//
//  Created by Victor Salazar on 28/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//
import Foundation
import CoreData
extension Event{
    @NSManaged var city_id:NSNumber
    @NSManaged var update_time:Date
    @NSManaged var date_time_finish:Date
    @NSManaged var date_time_start:Date
    @NSManaged var eveDescription:String
    @NSManaged var id:NSNumber
    @NSManaged var name:String
    @NSManaged var number_guests:NSNumber
    @NSManaged var photo_url:String
    @NSManaged var type:String
    @NSManaged var statistics:EventStatistics
    @NSManaged var disco:Disco
    @NSManaged var is_my_event:NSNumber
    @NSManaged var show_event:NSNumber
    @NSManaged var closed_lists:NSNumber
    @NSManaged var photos:NSOrderedSet?
    @NSManaged var public_promoters:NSNumber
}
