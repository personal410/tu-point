//
//  MyEventsViewController.swift
//  Tu Point
//
//  Created by victor salazar on 4/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyEventsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var arrMyEvents:Array<Event> = []
    //MARK: - IBOutlet
    @IBOutlet weak var myEventsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicParams = ["action": "getMyEvents", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrDicEventsTemp = dicResult["events"] as? Array<Dictionary<String, AnyObject>> {
                                let currentEvents = Event.getEvents()!
                                var currentEventIds = currentEvents.map(){$0.id.intValue}
                                for dicEvent in arrDicEventsTemp {
                                    let id = (dicEvent["id"] as! NSString).integerValue
                                    if let index = currentEventIds.index(of: id) {
                                        currentEventIds.remove(at: index)
                                        _ = Event.editEvent(dicEvent)
                                    }else{
                                        _ = Event.createEvent(dicEvent)
                                    }
                                    Event.setMyEventToEventId(id)
                                }
                            }
                            self.arrMyEvents = Event.getMyEvents()!
                            self.myEventsTableView.reloadData()
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? EventDetailViewController {
            viewCont.event = self.arrMyEvents[(myEventsTableView.indexPathForSelectedRow! as NSIndexPath).row]
        }
    }
    //MARK: - TableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrMyEvents.count
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myEventCell", for: indexPath) as! MyEventTableViewCell
        let myEvent = self.arrMyEvents[(indexPath as NSIndexPath).row]
        let dateFormatter = ToolBox.getDateFormatter()
        let myEventDate = myEvent.date_time_start
        dateFormatter.dateFormat = "EEEE"
        let dayWeek = dateFormatter.string(from: myEventDate).capitalized
        dateFormatter.dateFormat = "MMMM"
        let month = dateFormatter.string(from: myEventDate).capitalized
        dateFormatter.dateFormat = "dd 'de'"
        cell.dateLbl.text = "\(dayWeek) \(dateFormatter.string(from: myEventDate)) \(month)"
        cell.eventImgView.image = nil
        cell.eventImgView.strUrl = myEvent.photo_url
        return cell
    }
}
