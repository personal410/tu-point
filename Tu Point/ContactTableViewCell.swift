//
//  ContactTableViewCell.swift
//  Tu Point
//
//  Created by Victor Salazar on 3/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class ContactTableViewCell:UITableViewCell{
    @IBOutlet weak var contactImgView:FBImageView!
    @IBOutlet weak var contactNameLbl:UILabel!
    @IBOutlet weak var addButton:UIButton!
    @IBOutlet weak var VIPButton:UIButton!
}
