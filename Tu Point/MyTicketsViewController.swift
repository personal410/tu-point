//
//  MyEntriesViewController.swift
//  Tu Point
//
//  Created by victor salazar on 28/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyTicketsViewController:CustomViewController, UITableViewDataSource, UITableViewDelegate {
    //MARK: - Variables
    var arrTicketConfirmeds = Array<Dictionary<String, AnyObject>>()
    var arrTicketEarrings = Array<Dictionary<String, AnyObject>>()
    //MARK: - IBOutlet
    @IBOutlet weak var myTicketsTableView:UITableView!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        let dicParams = ["action": "getMyTickets", "user_id": "\(User.getUser()!.id.intValue)"]
        let strParams = ToolBox.convertDictionaryToGetParams(dicParams)
        SVProgressHUD.show(withStatus: "Cargando")
        ServiceConnector.connectToUrl("\(URLs.rootGetURL)\(strParams)", method: "GET", response:{(result:AnyObject?, error:NSError?) -> Void in
            SVProgressHUD.dismiss()
            if error == nil {
                if let dicResult = result as? Dictionary<String, AnyObject> {
                    if let state = dicResult["state"] as? String {
                        if state == "success" {
                            if let arrTicketConfirmedsTemp = dicResult["confirmed"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrTicketConfirmeds = arrTicketConfirmedsTemp
                            }
                            if let arrTicketEarringsTemp = dicResult["earrings"] as? Array<Dictionary<String, AnyObject>> {
                                self.arrTicketEarrings = arrTicketEarringsTemp
                            }
                            self.myTicketsTableView.reloadData()
                            return
                        }else{
                            let message = dicResult["message"] as! String
                            ToolBox.showAlertWithTitle("Error", withMessage: message, inViewCont: self)
                        }
                    }
                }
            }
            ToolBox.showErrorConnectionInViewCont(self)
        })
    }
    override func prepare(for segue:UIStoryboardSegue, sender:Any?){
        if let viewCont = segue.destination as? MyTicketViewController {
            let indexPath = self.myTicketsTableView.indexPathForSelectedRow!
            let dicTicket = self.arrTicketConfirmeds[(indexPath as NSIndexPath).row]
            viewCont.dicTicket = dicTicket
        }
    }
    //MARK: - TableView
    func numberOfSections(in tableView:UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 30
    }
    func tableView(_ tableView:UITableView, viewForHeaderInSection section:Int) -> UIView? {
        let height = self.tableView(tableView, heightForHeaderInSection: section)
        let view = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.width, height: height))
        let lbl = UILabel(frame: CGRect(x: 10, y: 0, width: tableView.frame.width - 20, height: height))
        lbl.font = UIFont(name: "MuseoSans-300", size: 15)
        lbl.text = section == 0 ? "Confirmados" : "Pendientes"
        lbl.textColor = UIColor.white
        view.addSubview(lbl)
        let lineView = UIView(frame: CGRect(x: 0, y: height - 1, width: tableView.frame.width, height: 1))
        lineView.backgroundColor = UIColor(rgb: 145)
        view.addSubview(lineView)
        return view
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        if section == 0 {
            return self.arrTicketConfirmeds.count
        }else{
            return self.arrTicketEarrings.count
        }
    }
    func tableView(_ tableView:UITableView, cellForRowAt indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myTicketCell", for: indexPath) as! MyTicketTableViewCell
        cell.isUserInteractionEnabled = (indexPath as NSIndexPath).section == 0
        if (indexPath as NSIndexPath).section == 0 {
            let dicTicket = self.arrTicketConfirmeds[(indexPath as NSIndexPath).row]
            let dicEvent = dicTicket["event"] as! Dictionary<String, AnyObject>
            let strDateTimeStart = dicEvent["date_time_start"] as! String
            let dateTimeStart = ToolBox.convertStringToDate(strDateTimeStart)
            let strDateTimeStartFinal = ToolBox.convertDateToString(dateTimeStart, withFormat: "d 'de' MMMM 'del' YYYY")
            var arrStrDateTimeStartFinal = strDateTimeStartFinal.components(separatedBy: " ")
            arrStrDateTimeStartFinal[2] = arrStrDateTimeStartFinal[2].capitalized
            cell.dateLbl.text = arrStrDateTimeStartFinal.joined(separator: " ")
            cell.eventImgView.image = nil
            cell.eventImgView.strUrl = dicEvent["photo_url"] as! String
            let dicDisco = dicEvent["disco"] as! Dictionary<String, String>
            cell.titleLbl.text = dicDisco["name"]
            cell.desciptionLbl.text = dicEvent["name"] as? String
        }else{
            
        }
        return cell
    }
}
