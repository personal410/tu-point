//
//  EventTableViewCell.swift
//  Tu Point
//
//  Created by victor salazar on 20/02/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class EventTableViewCell:UITableViewCell{
    @IBOutlet weak var eventImgView:LoadingImageView!
    @IBOutlet weak var discoNameLbl:UILabel!
    @IBOutlet weak var dayWeekLbl:UILabel!
    @IBOutlet weak var dayLbl:UILabel!
    @IBOutlet weak var monthLbl:UILabel!
    @IBOutlet weak var editEventCtrl:UIControl!
}