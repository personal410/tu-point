//
//  MyListDetailTableViewCell.swift
//  Tu Point
//
//  Created by victor salazar on 6/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class MyListDetailTableViewCell:UITableViewCell{
    @IBOutlet weak var senderLbl:UILabel!
    @IBOutlet weak var numberGuestLbl:UILabel!
    @IBOutlet weak var rightSpaceCons:NSLayoutConstraint!
    @IBOutlet weak var listStatusImgView:UIImageView!
}
