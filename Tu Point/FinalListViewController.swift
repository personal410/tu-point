//
//  FinalListViewController.swift
//  Tu Point
//
//  Created by Victor Salazar on 9/03/16.
//  Copyright © 2016 victor salazar. All rights reserved.
//
import UIKit
class FinalListViewController:CustomViewController, UITextFieldDelegate {
    //MARK: - Variables
    var arrCurrentDicGuests:Array<Dictionary<String, AnyObject>> = []
    var arrDicGuests:Array<Dictionary<String, AnyObject>> = []
    var eventList:EventList!
    //MARK: - IBOutlet
    @IBOutlet weak var searchTxtFld:UITextField!
    @IBOutlet weak var contactsTableView:UITableView!
    @IBOutlet weak var alreadyHereLbl:UILabel!
    @IBOutlet weak var willAssistLbl:UILabel!
    //MARK: - ViewCont
    override func viewDidLoad(){
        super.viewDidLoad()
        self.arrDicGuests = ToolBox.orderDicObjectsByName(arrDicObjects: eventList.arrAcceptedGuest)
        arrDicGuests = ToolBox.orderDicObjectsByName(arrDicObjects: arrDicGuests)
        self.searchTxtFld.leftViewMode = .always
        let view = UIView(frame: CGRect(x: 0, y: 0, width: 40, height: 35))
        let imgView = UIImageView(image: UIImage(named: "search"))
        imgView.frame.origin = CGPoint(x: 15, y: 8)
        view.addSubview(imgView)
        self.searchTxtFld.leftView = view
        NotificationCenter.default.addObserver(self, selector: #selector(FinalListViewController.showKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(FinalListViewController.hideKeyboard(_:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        self.arrCurrentDicGuests = self.arrDicGuests
        var count = 0
        for dicGuest in self.arrDicGuests {
            let state = dicGuest["state"]
            var finalState = false
            if let numberState = state as? NSNumber {
                finalState = numberState.boolValue
            }else if let strState = state as? NSString {
                finalState = strState.boolValue
            }
            if finalState {
                count += 1
            }
        }
        self.alreadyHereLbl.text = "\(count)"
        self.willAssistLbl.text = "\(self.arrDicGuests.count)"
    }
    //MARK: - Keyboard
    func showKeyboard(_ notification:Notification){
        var info = (notification as NSNotification).userInfo!
        let keyboardSize = (info[UIKeyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let keyboardHeight = keyboardSize.height
        let contentInset = UIEdgeInsets(top: 0.0, left: 0.0, bottom: keyboardHeight, right: 0)
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    func hideKeyboard(_ notification:Notification){
        let contentInset = UIEdgeInsets.zero
        self.contactsTableView.contentInset = contentInset
        self.contactsTableView.scrollIndicatorInsets = contentInset
    }
    //MARK: - UITableView
    func tableView(_ tableView:UITableView, heightForHeaderInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, heightForFooterInSection section:Int) -> CGFloat {
        return 0.1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrCurrentDicGuests.count
    }
    func tableView(_ tableView:UITableView, cellForRowAtIndexPath indexPath:IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactTableViewCell
        let dicGuest = self.arrCurrentDicGuests[(indexPath as NSIndexPath).row]
        cell.contactNameLbl.text = dicGuest["name"] as? String
        cell.contactImgView.strUrl = dicGuest["photo_url"] as! String
        let state = dicGuest["state"]
        var finalState = false
        if let numberState = state as? NSNumber {
            finalState = numberState.boolValue
        }else if let strState = state as? NSString {
            finalState = strState.boolValue
        }
        cell.addButton.isHidden = !finalState
        return cell
    }
    //MARK: - TextField
    func textFieldShouldReturn(_ textField:UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textField(_ textField:UITextField, shouldChangeCharactersIn range:NSRange, replacementString string:String) -> Bool {
        let newString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        if newString.characters.count == 0 {
            self.arrCurrentDicGuests = self.arrDicGuests
        }else{
            self.arrCurrentDicGuests = self.arrDicGuests.filter(){($0["name"] as! String).range(of: newString, options: .caseInsensitive, range: nil, locale: nil) != nil}
        }
        self.contactsTableView.reloadData()
        return true
    }
}
